import { connect } from "react-redux";
import { showModal } from "@store/actions/modal";
import VisitSite from "./VisitSiteSVG";
import css from "./HowWorks.module.scss";
import cx from "classnames/bind";
// import step1 from "./step1.svg";
import step2 from "./step2.svg";
import step3 from "./step3.svg";
import step4 from "./step4.svg";

function Step({ num, svgImg, desc }) {
  return (
    <div className={css.step}>
      <div className={css.step__num}>{num}</div>
      <img className={css.icon} src={svgImg} alt="" />
      <div className={css.step__desc}>{desc}</div>
    </div>
  );
}

const HowWorks = ({ showModal }) => {
  return (
    <div className="">
      <h1 className={css.header}>Как это работает</h1>
      <div className={css.stepsList}>
        <Step
          num="01"
          svgImg={step2}
          desc={
            <span>
              Пройди <a onClick={() => showModal("REGISTER_MODAL")}>легкую</a>{" "}
              регистрацию
            </span>
          }
        />
        <Step
          num="02"
          svgImg={step3}
          desc={
            <span>
              <a>Выбери</a> путевку и билет
            </span>
          }
        />
        <Step
          num="03"
          svgImg={step4}
          desc={
            <span>
              Путешевствуй <a>всего за 1 Евро</a>
            </span>
          }
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  modalType: state.modal.modalType,
});

export default connect(mapStateToProps, { showModal })(HowWorks);
