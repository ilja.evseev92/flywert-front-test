function NewSVG() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="33"
      height="10"
      fill="none"
      viewBox="0 0 33 10"
    >
      <path
        fill="#EB5757"
        d="M8.425.9V10H6.696L2.159 4.475V10H.079V.9h1.742l4.524 5.525V.9h2.08zm9.199 7.41V10h-7.046V.9h6.877v1.69h-4.784v1.976h4.225v1.638h-4.225V8.31h4.953zM32.927.9L29.95 10h-2.262l-2.002-6.162L23.619 10h-2.25L18.38.9h2.185l2.054 6.396L24.763.9h1.95l2.08 6.448L30.912.9h2.015z"
      ></path>
    </svg>
  );
}

export default NewSVG;
