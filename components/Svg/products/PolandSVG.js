import React from "react";

function Icon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="36"
      height="36"
      fill="none"
      viewBox="0 0 36 36"
    >
      <circle cx="18" cy="18" r="17.5" stroke="#F0F0F0"></circle>
    </svg>
  );
}

export default Icon;
