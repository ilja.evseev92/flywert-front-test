import ReactPlayer from 'react-player/youtube';
import css from './BannerHome.module.scss';

export default function BannerHome({ title }) {
  return (
    <div className={css.bannerHome}>
      {/* <h1 className={css.header}>Жить и путешествовать по&nbsp;Европе</h1> */}
      <div className={css.videoContainer}>
        <ReactPlayer
          className={css.video}
          width="100%"
          height="100%"
          url="https://www.youtube.com/watch?v=YAGeohZE0vQ&feature=emb_logo&ab_channel=FlyWertFlywert"
        />
      </div>
    </div>
  );
}
