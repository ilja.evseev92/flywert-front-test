import React from "react"
import Link from 'next/link'
import cx from 'classnames/bind'
import css from './BreadCrumbs.module.scss'

const BreadCrumbs = props => {
  const {breadCrumbParent, breadCrumbActive, className} = props;

  return (
      <div className={cx(css.breadcrumbs, className)}>
        <ul>
          <li className={css.breadcrumbs__active}>
            <Link href="/">
              <a>Главная</a>
            </Link>
          </li>
          {breadCrumbParent ? <li className={css.breadcrumbs__item}>{breadCrumbParent}</li> : ''}
          {breadCrumbActive ? <li className={css.breadcrumbs__item}>{breadCrumbActive}</li> : ''}
        </ul>
      </div>
  )
}

export default BreadCrumbs
