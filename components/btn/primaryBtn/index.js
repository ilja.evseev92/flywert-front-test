import css from './Btn.module.scss';

export default function PrimaryBtn(props) {
  return (
    <button
      className={`${css.primary__btn} ${props.className || ''}`}
      onClick={props.onClick}
    >
      <span> {props.name} </span>
    </button>
  );
}
