import {useState} from 'react'
import {connect} from 'react-redux'
import cx from 'classnames/bind'
import UserList from './userList'
import MessageList from "./messageList";
import css from './Chat.module.scss'

const Chat = ({userList, messageList}) => {
  const [messageListHeight, setMessageListHeight] = useState(500)

  const messageListHeightChange = height => {
    setMessageListHeight(height)
  }

  return (
      <div className={css.chat}>
        <div className={css.chat__title}>
          <span>Чат</span>
          <img src="/img/pages/chat/chat.png" alt="chat"/>
        </div>
        <div className={cx(css.chat__inner, "flex")}>
          <UserList userList={userList} heightContent={messageListHeight}/>
          <MessageList messageList={messageList} messageListHeightChange={messageListHeightChange} />
        </div>
      </div>
  )
}

const mapStateToProps = state => (
    {
      userList: state.chat.userList,
      messageList: state.chat.messageList,
    }
)

export default connect(mapStateToProps)(Chat);