import {useRef, useState} from 'react';
import cx from 'classnames/bind'
import css from './Chat.module.scss'
import Echo from 'laravel-echo';
import {token} from "@services/token";

const MessageList = ({messageList, messageListHeightChange}) => {
  const chatTextarea = useRef(null);
  const messageContent = useRef(null);
  const messageListRef = useRef(null);
  const [textareaHeight, setTextareaHeight] = useState(20)
  const [messageListHeight, setMessageListHeight] = useState(389)

  const handleSubmit = e => {
    e.preventDefault();
    if(chatTextarea?.current?.value?.length) {

      if(textareaHeight > 20) {
        messageListHeightChange(messageContent.current.offsetHeight - textareaHeight + 20)
        setTextareaHeight(20)
      }
      chatTextarea.current.value = ''
    }
  }

  const handleOnChange = e => {
    if(chatTextarea?.current?.scrollHeight && (textareaHeight < chatTextarea?.current?.scrollHeight)) {
      if(chatTextarea.current.offsetHeight < 100) {
        setTextareaHeight(chatTextarea?.current?.scrollHeight)

        if(messageListRef?.current?.offsetHeight) {
          setMessageListHeight(messageListRef.current.offsetHeight + 30)
        }
        if(messageContent?.current?.offsetHeight) {
          messageListHeightChange(messageContent.current.offsetHeight + 21)
        }
      }
    }

  }

  if (typeof window !== "undefined") {

    window.Pusher = require('pusher-js');
      window.Echo = new Echo({
        broadcaster: 'pusher',
        key: "b00c706951da7ae60de4",
        // authEndpoint: '/custom/endpoint/auth',
        wssHost: 'api.flywert-dev.ru/app/b00c706951da7ae60de4?protocol=7&client=js&version=7.0.1&flash=false',
        wsPort: 6001,
        wssPort: 443,
        forceTLS: false,
        disableStats: true,
        encrypted: true,
        enabledTransports: ['ws', 'wss'],
        auth: {
          headers: {
            Authorization: 'Bearer ' + token,
          },
        },
      });

      window.addEventListener('DOMContentLoaded',function () {
        window.channel('chat.ru').listen("SharingChatNewMessage", e => {
          console.log(e);
        //showMessage(e.message.from.first_name,e.message.created_at,e.message.text);
          window.Echo.join('chat.en').listen("App\Events\ChatNewMessage", (e) => {
            console.log(e);
          })
              .here((users)=>{
                console.log('Here:');
                console.log(users);
              })
              .joining((user)=>{
                console.log(`Пришёл ${user.first_name} ${user.last_name}`);
              })
              .leaving((user)=>{
                console.log(user);
              });

        });
    })
  }

  return (
      <div className={css.chat__content} ref={messageContent}>
        <div className={cx(css.chat__messageList, 'custom-scroll-y')} ref={messageListRef} style={{height: messageListHeight}}>
          {
            messageList.map(item => (
                <div key={item.id} className={css.chat__message}>
                  <div className={cx(css.chat__messageHeader, 'flex flex__y_center')}>
                    <img src={item.avatar} alt={item.name}/>
                    <span>{`${item.firstName} ${item.lastName}`}</span>
                  </div>
                  <div className={cx(css.chat__messageContent, item.status === 'in' ? css.chat__messageIn : css.chat__messageOut)}>
                    <p>{item.message}</p>
                  </div>
                  <div className={cx(css.chat__messageFooter, 'flex flex__x_end')}>
                    <p>{item.date}</p>
                  </div>
                </div>
            ))
          }
        </div>
        <div className={css.chat__form}>
          <form onSubmit={handleSubmit}>
            <div className={css.chat__form__inner}>
              <textarea
                  style={{height: `${textareaHeight}px`}}
                  rows="1"
                  placeholder="Ваше сообщение"
                  id="input-chat"
                  className="input-chat custom-scroll-y"
                  name="message"
                  onChange={handleOnChange}
                  ref={chatTextarea}
              />
              <button type="submit">
                <span>Отправить</span>
                <img src="/img/pages/chat/sendMessage.svg" alt="send"/>
              </button>
            </div>
          </form>
        </div>
      </div>
  )
}

export default MessageList
