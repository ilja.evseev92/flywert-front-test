import Link from 'next/link'
import css from './FooterMenu.module.scss'
import footerCss from '../Footer.module.scss'
import useTranslation from '../../../hooks/useTranslation'

export default function FooterMenu() {
  const { t, locale } = useTranslation();
  const menuItems = [
    {title: t('footerMenuAbout'), link: `/${locale}/about`},
    {title: t('footerMenuContact'), link: `/${locale}/contact`},
    {title: t('footerMenuPartner'), link: `/${locale}/partner`},
    {title: t('footerMenuFaq'), link: `/${locale}/textPage`},
  ]

  return (
      <div>
        <p className={footerCss.footer__item_title}>
          {t('footerMenuTitle')}
        </p>
        <ul className={css.footer__menu_list}>
          {
            menuItems.map((item, index) => (
                <li key={index}>
                  <Link href={item.link}>
                    <a>{item.title}</a>
                  </Link>
                </li>
            ))
          }
        </ul>
      </div>
  )
}