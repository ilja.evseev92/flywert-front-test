import Image from 'next/image'
import footerCss from "../Footer.module.scss";
import css from './FooterPayment.module.scss';
import useTranslation from '../../../hooks/useTranslation'

export default function FooterPayment() {
  const { t } = useTranslation()
  const paymentItems = [
    {alt: 'MasterCard', src: '/img/icons/masterCard.svg', width: 42, height: 26},
    {alt: 'Visa', src: '/img/icons/visa.svg', width: 46, height: 15},
    {alt: 'Amex', src: '/img/icons/amex.svg', width: 52, height: 34},
    {alt: 'Paypal', src: '/img/icons/payPal.svg', width: 67, height: 17},
    {alt: 'Sberbank', src: '/img/icons/sberbank.svg', width: 78, height: 22},
  ]
  return (
      <div className={css.footer__payment}>
        <p className={footerCss.footer__item_title}>{t('footerPaymentTitle')}</p>
        <ul className={css.footer__payment_items}>
          {
            paymentItems.map((item, index) => (
                <li key={index}>
                  <Image  {...item} key={index} />
                </li>
            ))
          }
        </ul>
      </div>
  )
}