import { connect } from "react-redux";
import UserInfo from "./userInfo";
import PrimaryBtn from "@components/btn/primaryBtn";
import {LogoutAction} from '@store/actions/auth'
import useTranslation from '@hooks/useTranslation'
import css from '../Header.module.scss';
import cx from "classnames/bind";

const AuthHeader = (props) => {
  const { t } = useTranslation();

  return (
    <div className={cx(css.user__info_group, "flex flex__x_end flex__y_center")}>
      <UserInfo userData={props.userData} />
      <PrimaryBtn
        name={t("headLogoutBtn")}
        onClick={props.LogoutAction}
        className={cx(css.header__btn, css.header__blue_btn)}
      />
    </div>
  );
};

const mapStateToProps = state => ({
  userData: state.user.userData
})

export default connect(mapStateToProps, { LogoutAction })(AuthHeader);
