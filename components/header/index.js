import { useState, useEffect } from "react";
import cx from "classnames/bind";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";
import css from "./Header.module.scss";
import Logo from "@components/logo";
import { showModal, hideModal } from "@store/actions/modal";
import NotAuthHeader from "./notAuthHeader";
import AuthHeader from "./authHeader";
import LangSelect from "@components/header/langSelect";
import MenuToggle from "./menuToggle";
import { getAddressText } from "@helpers/utils";

const Header = ({ showModal, hideModal, modalType, isAuth }) => {
  const [address, setAddress] = useState("");

  useEffect(() => {
    if (process.browser) setAddress(getAddressText());
    window.onresize = function (event) {
      if (modalType === "MOB_MENU_MODAL" && window.innerWidth > 767) {
        hideModal();
      }
    };
  }, [showModal, hideModal, modalType, isAuth]);

  return (
    <header className={css.header}>
      <div className={cx(css.header__container, "container")}>
        <MenuToggle
          showModal={showModal}
          modalType={modalType}
          hideModal={hideModal}
        />
        <Logo className={css.header__logo} />
        <div className={cx(css.header__inner, "w100")}>
          {address && isAuth && (
            <div className={cx(css.address__wrap, "flex flex__x_end")}>
              <div className={css.address}>{address}</div>
            </div>
          )}
          <div className="flex w100 flex__y_center">
            <div className={css.header__services}>
              {isAuth ? (
                <AuthHeader />
              ) : (
                <NotAuthHeader showModal={showModal} />
              )}
            </div>
          </div>
        </div>
        {!isMobile && <LangSelect />}
        {isMobile && <LangSelect />}
      </div>
    </header>
  );
};

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
  modalType: state.modal.modalType,
});

export default connect(mapStateToProps, { showModal, hideModal })(Header);
