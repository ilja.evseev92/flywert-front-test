import {useState, useEffect} from 'react'
import React from 'react'
import Select from 'react-select';
import { useRouter } from 'next/router'
import { locales } from '@translations/config'
import { LocaleContext } from '../../../context/LocaleContext'
import css from '../Header.module.scss';
import {styles} from './settings'

export default function LangSelect() {
  const router = useRouter()
  const { locale } = React.useContext(LocaleContext)
  const [selectedOption, setSelectedOption] = useState({value: locale, label: locale.toUpperCase()});

  useEffect(() => {
      setSelectedOption({value: locale, label: locale.toUpperCase()})
  }, [locale])

  const handleLocaleChange = React.useCallback(
      (data) => {
        const regex = new RegExp(`^/(${locales.join('|')})`)
        router.push(router.pathname, router.asPath.replace(regex, `/${data.value}`))
        setSelectedOption({value: data.value, label: data.value.toUpperCase()})
      },
      [router]
  )

  const options = locales.map((item) => (
      { value: item, label: item.toUpperCase() }
  ))

  return (
      <Select
          value={selectedOption}
          onChange={handleLocaleChange}
          options={options}
          inputId="langSelectInput"
          styles={styles}
          isSearchable={false}
          className={css.lang__select}
      />
  )
}