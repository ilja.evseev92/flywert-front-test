import cx from 'classnames/bind';
import PrimaryBtn from "@components/btn/primaryBtn";
import css from '../Header.module.scss';
import useTranslation from '../../../hooks/useTranslation'

export default function NotAuthHeader({showModal}) {
  const { t } = useTranslation()
  return (
      <>
        <PrimaryBtn
            name={t('register')}
            onClick={() => showModal('REGISTER_MODAL')}
            className={cx(css.header__btn, css.header__white_btn)}
        />
        <PrimaryBtn
            name={t('headLoginBtn')}
            onClick={() => showModal('LOGIN_MODAL')}
            className={cx(css.header__btn, css.header__blue_btn)}
        />
      </>
  )
}