import React from "react";
import {connect} from 'react-redux'
import Cookies from "js-cookie";
import Head from "next/head";
import Header from "@components/header";
import Footer from "@components/footer";
import ModalRoot from "@components/modal";
import useTranslation from "../../hooks/useTranslation";
import {showModal} from '@store/actions/modal'
import {getUserAddress} from '@helpers/utils'

const Layout = (props) => {
  const {children, titleKey, isAuthenticated, isAuth, showModal} = props;

  if(isAuth) {

    const success = ({ coords }) => {

      Cookies.set("lat", coords.latitude);
      Cookies.set("lng", coords.longitude);

      getUserAddress().then(res => {
        if(res) {
          showModal('ADDRESS_INFO_MODAL')
        }
      }).catch(err => {
        console.log("err", err)
      })

    };

    const onError = () => {
      showModal('POSITION_MODAL')
    };

    if (!(Cookies.get("lat") && Cookies.get("lng"))) {
      const geo = navigator.geolocation;
      if (!geo) {
        showModal('POSITION_MODAL')
      } else {
        geo.getCurrentPosition(success, onError);
      }
    }

  }
  const { t } = useTranslation()

  return (
    <>
      <Head>
        <title>{t(titleKey)}</title>
        <meta charSet="UTF-8" />
      </Head>
      <div className="baseLayer">
        <Header isAuthenticated={isAuthenticated} />
        <main className="main">
          <div className="container">{children}</div>
        </main>
        <ModalRoot isAuthenticated={isAuthenticated} />
        <Footer />
      </div>
    </>
  );
}

const mapStateToProps = state => ({
  isAuth: state.auth.isAuth,
})

export default connect(mapStateToProps, {showModal})(Layout)
