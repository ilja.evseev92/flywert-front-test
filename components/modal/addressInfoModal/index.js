import React, { useState } from "react";
import Cookies from "js-cookie";
import { algoliaPlacesInput } from "@helpers/utils";
import cx from "classnames/bind";
import css from "./AddressInfoModal.module.scss";
import { locales } from "@translations/config";
import { useRouter } from "next/router";

const AddressInfoModal = ({ hideModal, showModal }) => {
  const cityName = localStorage.getItem("city_name");
  const localeName = localStorage.getItem("locale_name");
  const [showAddressInput, setShowAddressInput] = useState(false);
  const router = useRouter();

  const successBtnHandleClick = React.useCallback(() => {
    const regex = new RegExp(`^/(${locales.join("|")})`);
    router.push(
      router.pathname,
      router.asPath.replace(regex, `/${Cookies.get("locale")}`)
    );
    hideModal();
  }, [router]);

  const handleShowAddressInput = () => {
    setShowAddressInput(true);
  };

  return (
    <div className={css.wrapper}>
      <div className={css.wrapper__title}>
        <p>Ваш адрес</p>
      </div>
      {!showAddressInput && (
        <>
          <div className={css.wrapper__subtitle}>
            <p>{`${localeName} ${cityName}`}</p>
          </div>
          <div className={css.btnGroup}>
            <button
              className={cx(css.btn, css.success_btn)}
              onClick={successBtnHandleClick}
            >
              Да, все верно
            </button>
            <button
              className={cx(css.btn, css.error_btn)}
              onClick={handleShowAddressInput}
            >
              Ввести вручную
            </button>
          </div>
        </>
      )}

      {showAddressInput && (
        <div className={css.wrapper__search}>
          {algoliaPlacesInput(hideModal)}
        </div>
      )}
    </div>
  );
};

export default AddressInfoModal;
