import { useEffect } from "react";
import { connect } from "react-redux";
import Modal from "react-modal";
import { hideModal, showModal } from "@store/actions/modal";
import { clearRegisterData } from "@store/actions/auth";
import { clearErrorMessage } from "@store/actions/helpers";
import { setSelectedTicket, setTicketData } from "@store/actions/tickets";
import { setPaymentRequestMessage } from "@store/actions/payment";
import LoginModal from "./loginModal";
import RegisterModal from "./registerModal";
import MobMenuModal from "./mobMenuModal";
import ErrorModal from "./errorModal";
import PaymentModal from "./paymentModal";
import TicketsModal from "./ticketsModal";
import PositionModal from "./positionModal";
import NotAuthorized from "./notAuthorized";
import AddressInfoModal from "./addressInfoModal";

const modalWidth = {
  LOGIN_MODAL: 770,
  REGISTER_MODAL: 770,
  MOB_MENU_MODAL: "100%",
  ERROR_MODAL: 500,
  PAYMENT_MODAL: 970,
  POSITION_MODAL: 970,
  ADDRESS_INFO_MODAL: 970,
  TICKETS_MODAL: 1168,
  NOT_AUTHORIZED: 1168,
};

Modal.setAppElement("#__next");

const ModalRoot = (props) => {
  const { modalType, isAuth } = props;

  const MODAL_COMPONENTS = {
    LOGIN_MODAL: {
      component: LoginModal,
      clearDataAction: props.clearRegisterData,
    },
    REGISTER_MODAL: {
      component: RegisterModal,
      clearDataAction: props.clearRegisterData,
    },
    MOB_MENU_MODAL: {
      component: MobMenuModal,
    },
    ERROR_MODAL: {
      component: ErrorModal,
      clearDataAction: props.clearErrorMessage,
    },
    POSITION_MODAL: {
      component: PositionModal,
    },
    ADDRESS_INFO_MODAL: {
      component: AddressInfoModal,
    },
    PAYMENT_MODAL: {
      component: PaymentModal,
      clearDataAction: props.setPaymentRequestMessage,
    },
    TICKETS_MODAL: {
      component: TicketsModal,
      clearDataAction: () => {
        props.setSelectedTicket();
        props.setTicketData("");
      },
    },
    NOT_AUTHORIZED: {
      component: NotAuthorized,
    },
  };

  if (!modalType) {
    return null;
  }
  const SpecificModal = MODAL_COMPONENTS[modalType].component;
  const clearModalData = MODAL_COMPONENTS[modalType].clearDataAction;
  const marginTopModal = modalType === "MOB_MENU_MODAL" ? "55px" : "13px";

  let customStyles = {
    overlay: {
      backgroundColor: "rgba(34, 34, 34, 0.4)",
    },
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      marginRight: "-50%",
      transform: "translate(-50%, -50%)",
      padding: 0,
      border: "none",
      background: "none",
      width: "100%",
      maxWidth: "970px",
      WebkitOverflowScrolling: "touch",
      borderRadius: "10px",
    },
  };

  useEffect(() => {
    switch (modalType) {
      case "MOB_MENU_MODAL":
        return (customStyles.content = {
          position: "relative",
          top: "0",
          left: "0",
          right: "0",
          bottom: "0",
          border: "none",
          background: "none",
        });
      case "LOGIN_MODAL":
        return (customStyles.content["maxWidth"] = modalWidth.LOGIN_MODAL);
      case "REGISTER_MODAL":
        return (customStyles.content["maxWidth"] = modalWidth.REGISTER_MODAL);
      case "ERROR_MODAL":
        return (customStyles.content["maxWidth"] = modalWidth.ERROR_MODAL);
      case "PAYMENT_MODAL":
        return (customStyles.content["maxWidth"] = modalWidth.PAYMENT_MODAL);
      case "TICKETS_MODAL":
        return (customStyles.content["maxWidth"] = modalWidth.TICKETS_MODAL);
      case "NOT_AUTHORIZED":
        return (customStyles.content["maxWidth"] = modalWidth.TICKETS_MODAL);
      case "POSITION_MODAL":
        return (customStyles.content["maxWidth"] = modalWidth.POSITION_MODAL);
      case "ADDRESS_INFO_MODAL":
        return (customStyles.content["maxWidth"] =
          modalWidth.ADDRESS_INFO_MODAL);
      default:
        return (customStyles.content["maxWidth"] = "100%");
    }
  }, [modalType]);

  const modalClose = () => {
    props.hideModal();
    if (clearModalData) clearModalData();
  };

  return (
    <Modal
      isOpen={!!SpecificModal}
      onRequestClose={modalClose}
      style={customStyles}
    >
      <SpecificModal
        hideModal={props.hideModal}
        showModal={props.showModal}
        clearModalData={clearModalData}
        isAuth={isAuth}
      />
      <style jsx global>{`
        .ReactModal__Content {
          max-width: calc(100vw - 2rem);
          max-height: calc(100vh - 2rem);
          margin-top: ${marginTopModal};
          position: relative;
        }
      `}</style>
    </Modal>
  );
};

const mapStateToProps = (state) => ({
  modalType: state.modal.modalType,
  isAuth: state.auth.isAuth,
});

export default connect(mapStateToProps, {
  hideModal,
  showModal,
  clearRegisterData,
  clearErrorMessage,
  setSelectedTicket,
  setPaymentRequestMessage,
  setTicketData,
})(ModalRoot);
