import cx from 'classnames/bind'
import Image from "next/image";
import css from './NotAuthorized.module.scss'
import rootModalCss from "@components/modal/RootModal.module.scss";

const NotAuthorized = ({hideModal, showModal}) => {
  return (
      <div className={css.notAuthModal}>
        <span className={cx(rootModalCss.auth__modal_close, css.notAuthModal__close)} onClick={hideModal}>
          <Image src="/img/icons/closeModal.svg" alt="Close" width={15} height={15} />
        </span>
        <div className={css.notAuthModal__content}>
          <p>Вы должны быть авторизованы.</p>
          <div className={css.notAuthModal__btn_group}>
            <button onClick={() => showModal('LOGIN_MODAL')}>Войти</button>
            <button onClick={() => showModal('REGISTER_MODAL')}>Регистрация</button>
          </div>
        </div>
      </div>
  )
}

export default NotAuthorized
