import css from './paymentModal.module.scss';

const PaymentBalance = props => {
  const {currency, balance} = props;

  return (
      <div className={css.balance}>
        <p className={css.balance__inner}>{`Баланс: ${balance} ${currency || ''}`}</p>
      </div>
  )
}


export default PaymentBalance