import {useState} from 'react';
import {connect} from 'react-redux'
import cx from "classnames/bind";
import css from "./paymentModal.module.scss"
import {hideModal} from "@store/actions/modal";
import {setSelectedTicket} from '@store/actions/tickets'
import {setPaymentRequestMessage} from '@store/actions/payment'
import {orderProcess} from '@store/actions/payment'
import WaysPayment from "./waysPayment"
import rootModalCss from "@components/modal/RootModal.module.scss";


const PaymentModal = props => {
  const {
    hideModal,
    selectedTicketData,
    setSelectedTicket,
    paymentTypes,
    userData,
    orderProcess,
    paymentRequest,
    setPaymentRequestMessage,
    loading
  } = props;

  const {name, price, currency, id} = selectedTicketData
  const [selectedPaymentType, setSelectedPaymentType] = useState('')
  const [paymentTypeId, setPaymentTypeId] = useState('')
  const [privacyPolicy, setPrivacyPolicy] = useState(false)


  let _ = require('lodash.foreach')
  let x;
  _(paymentTypes, function(value) {
    x = value.clientSecret
  });


  let requestObj;
  if (selectedPaymentType === 'stripe') {
    requestObj = {
        "payment_type_id": paymentTypeId,
        "product_id": id,
        "payment_token": x.substr(0, 27),
        "amount": price
      }
  } else {
    requestObj = {
        "payment_type_id": paymentTypeId,
        "product_id": id,
        "amount": price
      }
  }

  const handleCloseModal = () => {
    setSelectedTicket('')
    hideModal()
    if(paymentRequest.message) {
      setPaymentRequestMessage()
    }
  }

  const handleClickPay = () => {
    if(!selectedPaymentType) {
      setPaymentRequestMessage(false, 'Выберите тип оплаты')
    } else if(!privacyPolicy){
      setPaymentRequestMessage(false, 'Подтвердите согласие с правилами сайта')
    }

    if(paymentTypeId && privacyPolicy && selectedPaymentType && id) {
      orderProcess(requestObj)
      //console.log('requestObj', requestObj)
      //console.log('paymentTypes', paymentTypes)
      //console.log('selectedPaymentType', selectedPaymentType)
      //console.log('reqObj', reqObj)
    }
  }

  return (
      <div className={css.payment}>
        <div className={cx(css.payment__header, 'flex flex__x_end')}>
          <img src="/img/icons/errorModalClose.svg" alt="close" onClick={handleCloseModal} />
        </div>
        <div className={cx(css.payment__content)}>
          <div className={css.payment__headerTitle}>
            <p className={css.payment__title}>Оформление участия</p>
            <p className={css.payment__subTitle}>Ваш заказ</p>
          </div>
          <div className={cx(css.payment__message, paymentRequest.message && cx(rootModalCss.form__message_block, rootModalCss.error__message))}>
            {paymentRequest.message}
          </div>
          <div className={css.payment__order}>
            <ul>
              <li><span>Товар:</span> {name || ''}</li>
              <li><span>Подытог:</span> {price || ''} {currency || ''}</li>
              <li><span>Итого:</span> {price || ''} {currency || ''}</li>
            </ul>
          </div>
          <div className={css.payment__wrapper}>
            <div className={css.payment__waysPayment}>
              <WaysPayment
                  paymentTypes={paymentTypes}
                  userData={userData}
                  selectedPaymentType={selectedPaymentType}
                  setSelectedPaymentType={setSelectedPaymentType}
                  setPaymentTypeId={setPaymentTypeId}
                  paymentRequest={paymentRequest}
                  setPaymentRequestMessage={setPaymentRequestMessage}
              />
            </div>
            <div className={css.payment__privacy_policy}>
              <p>
                Ваши личные данные будут использоваться для обработки ваших заказов, упрощения
                вашей работы с сайтом и для других целей, описанных в нашей Политике
                конфиденциальности.
              </p>
              <div>
                <input
                    id="privacyPolicy"
                    className={css.checkboxCustom}
                    type="checkbox"
                    checked={privacyPolicy}
                    onChange={() => setPrivacyPolicy(!privacyPolicy)}
                />
                <label htmlFor="privacyPolicy" className={css.checkboxCustomLabel}>Я прочитал(а) и соглашаюсь с правилами сайта.</label>
              </div>
            </div>
            <div className={cx(css.payment__button, loading ? 'btn__disabled' : '')}>
              <button onClick={handleClickPay}>Оплатить</button>
            </div>
          </div>
        </div>
      </div>
  )
}

const mapStateToProps = state => ({
  errorMessage: state.helpers.errorMessage,
  selectedTicketData: state.tickets.selectedTicketData,
  paymentTypes: state.payment.paymentTypes,
  loading: state.payment.loading,
  paymentRequest: state.payment.paymentRequest,
  userData: state.user.userData,
})

export default connect(mapStateToProps, {hideModal, setSelectedTicket, orderProcess, setPaymentRequestMessage})(PaymentModal)
