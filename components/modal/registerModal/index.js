import {useState} from 'react'
import {connect} from 'react-redux';
import cx from 'classnames/bind';
import Image from "next/image";
import rootModalCss from '../RootModal.module.scss';
import css from './RegisterModal.module.scss';
import ModalSocialIcons from '../modalSocialIcons'
import RegisterSms from "./registerSms";
import RegisterEmail from './registerEmail'
import useTranslation from '../../../hooks/useTranslation'
import {authSetOneField} from "@store/actions/auth";

const RegisterModal = (props) => {
  const { t } = useTranslation();
  const localStorageActiveForm = localStorage.getItem('registerActiveForm');
  const [activeForm, setActiveForm] = useState(localStorageActiveForm || 'sms');

  const closeModal = () => {
    props.hideModal()
    if(props.clearModalData) props.clearModalData()
  }

  const renderForm = () => {
    if(activeForm === 'sms' && props.phoneRegActiveForm !== 'success') {
      return <RegisterSms />
    } else {
      return <RegisterEmail />
    }
  }

  const onChangeActiveForm = (type) => {
    setActiveForm(type)
    props.authSetOneField('message', '')
  }

  return (
      <div className={cx(rootModalCss.auth__modal, css.register__modal, 'flex__center' )}>
        <span className={rootModalCss.auth__modal_close} onClick={closeModal}>
          <Image src="/img/icons/closeModal.svg" alt="Close" width={15} height={15} />
        </span>
        <div className={rootModalCss.modal__content}>
          <h3 className={rootModalCss.modal__title}>{t('register')}</h3>
          <div className={(!props.requestStatus && props.message) && cx(rootModalCss.form__message_block, rootModalCss.error__message)}>
            {(!props.requestStatus && props.message) && props.message}
          </div>
          <div className={cx(css.select_button_items, 'flex row15')}>
            <button
                className={cx(css.reg__select_button, activeForm === 'sms' && css.btn__active)}
                onClick={() => onChangeActiveForm('sms')}
                disabled={activeForm === 'sms'}
            >
              {t('regModalSmsBtn')}
            </button>
            <button
                className={cx(css.reg__select_button, activeForm === 'email' && css.btn__active)}
                onClick={() => onChangeActiveForm('email')}
                disabled={activeForm === 'email'}
            >
              {t('regModalEmailBtn')}
            </button>
          </div>

          {renderForm()}

          <div className={rootModalCss.or__auth}>
            <p>{t('formOr')}</p>
          </div>
          <ModalSocialIcons />
          <div
              className={(props.requestStatus ? props.message : '') && cx(rootModalCss.form__message_block, rootModalCss.success__message)}
          >
            {(props.requestStatus ? props.message : '') && props.message}
          </div>
        </div>
      </div>
  )
}

export default connect(
    state => ({
      message: state.auth.message,
      requestStatus: state.auth.requestStatus,
      phoneRegActiveForm: state.auth.phoneRegActiveForm
    }), {authSetOneField}
)(RegisterModal);