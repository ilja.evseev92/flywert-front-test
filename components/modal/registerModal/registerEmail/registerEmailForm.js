import { useFormik } from 'formik';
import Link from 'next/link';
import * as Yup from 'yup';
import cx from 'classnames/bind';
import rootModalCss from '../../RootModal.module.scss';
import css from '../RegisterModal.module.scss';
import useTranslation from '../../../../hooks/useTranslation';

const RegisterEmailForm = ({
  btnTranslateKey,
  handleSubmit,
  loading,
  btnClassName,
}) => {
  const { t } = useTranslation();

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      password: '',
      password_confirmation: '',
      email: '',
      gender: '',
    },

    validationSchema: Yup.object({
      firstName: Yup.string().required('No password provided.'),
      lastName: Yup.string().required('No password provided.'),
      gender: Yup.string().required('gender required'),
      password: Yup.string()
        .required('Password is required')
        .min(6, 'Password is too short - should be 6 chars minimum.'),
      password_confirmation: Yup.string()
        .required('Password is required')
        .oneOf([Yup.ref('password'), null], 'Passwords must match'),
      email: Yup.string().email('Invalid email address').required('Required'),
    }),
    onSubmit: (values) => {
      handleSubmit(values);
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div className={cx(css.reg__form_items, 'flex flex-wrap row15')}>
        <div className={css.reg__form_item}>
          <input
            id="firstName"
            name="firstName"
            type="text"
            autoComplete="off"
            placeholder={t('firstName')}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.firstName}
            className={
              formik.touched.firstName &&
              formik.errors.firstName &&
              'input__error'
            }
          />
        </div>
        <div className={css.reg__form_item}>
          <input
            id="lastName"
            name="lastName"
            type="text"
            autoComplete="off"
            placeholder={t('lastName')}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.lastName}
            className={
              formik.touched.lastName &&
              formik.errors.lastName &&
              'input__error'
            }
          />
        </div>
        <div className={css.reg__form_item}>
          <input
            id="password"
            name="password"
            type="password"
            placeholder={t('password')}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.password}
            className={
              formik.touched.password &&
              formik.errors.password &&
              'input__error'
            }
          />
        </div>
        <div className={css.reg__form_item}>
          <input
            id="password_confirmation"
            name="password_confirmation"
            type="password"
            placeholder={t('regModalConfPassword')}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.password_confirmation}
            className={
              formik.touched.password_confirmation &&
              formik.errors.password_confirmation &&
              'input__error'
            }
          />
        </div>
        <div className={css.reg__form_item}>
          <input
            id="email"
            name="email"
            type="email"
            autoComplete="off"
            placeholder={t('email')}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.email}
            className={
              formik.touched.email && formik.errors.email && 'input__error'
            }
          />
        </div>
        <div className={css.reg__form_item}>
          <button
            className={cx(
              rootModalCss.submit__btn,
              btnClassName || '',
              loading || !(formik.isValid && formik.dirty)
                ? 'btn__disabled'
                : ''
            )}
            type="submit"
          >
            {t(btnTranslateKey)}
          </button>
        </div>
      </div>
      <div className={cx(css.gender__group, 'flex flex__x_center')}>
        <div className={cx(css.gender__item, 'flex flex__y_center')}>
          <input
            className={cx(
              css.gender__radio,
              formik.touched.gender && formik.errors.gender && 'input__error'
            )}
            id="male"
            type="radio"
            value="male"
            name="gender"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />

          <label htmlFor="male">{t('male')}</label>
        </div>
        <div className={cx(css.gender__item, 'flex flex__y_center')}>
          <input
            className={cx(
              css.gender__radio,
              formik.touched.gender && formik.errors.gender && 'input__error'
            )}
            id="female"
            type="radio"
            value="female"
            name="gender"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
          <label htmlFor="female">{t('female')}</label>
        </div>
      </div>

      <div className={css.privacy__policy}>
        <p>{t('clickRegisterBtn')}</p>
        <Link href="/">
          <a>{t('regModalPrivacyPolicy')}</a>
        </Link>
      </div>
    </form>
  );
};

export default RegisterEmailForm;
