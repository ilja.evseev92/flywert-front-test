import {useState} from 'react';
import cx from 'classnames/bind'
import useTranslation from '@hooks/useTranslation'
import css from '../RegisterSms.module.scss'
import rootModalCss from "@components/modal/RootModal.module.scss";

export default function ConfirmCode({setOneField, loading, confirmPhone, confirmCodeAction}) {
  const {t} = useTranslation();
  const [code, setCode] = useState('')

  const changePhone = () => {
    setOneField('phoneRegActiveForm', '')
  }

  const continueRegistration = () => {
    if(!btnDisabled()) confirmCodeAction({phone: confirmPhone, code: code})
  }

  const btnDisabled = () => {
    if(code.length < 4 || loading) return true
  }

  return (
      <div>
        <div className={cx(css.confirm__code, 'flex flex__x_center row15')}>
          <input
              value={code}
              onChange={e => setCode(e.target.value)}
              type="text"
              placeholder={t('smsCode')}
              className={css.confirm__code_input}
          />
          <button
              className={cx(rootModalCss.submit__btn, css.small_reg_btn, btnDisabled() ? 'btn__disabled' : '')}
              onClick={continueRegistration}
          >
            {t('regContinue')}
          </button>
        </div>
        <div className={css.change__phone}>
          <span onClick={changePhone}>{t('changePhone')}</span>
        </div>
      </div>
  )
}