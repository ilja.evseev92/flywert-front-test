import cx from "classnames/bind";
import PhoneInput from "react-phone-input-2";
import ru from "react-phone-input-2/lang/ru.json";
import de from "react-phone-input-2/lang/de.json";
import useTranslation from "../../../../../hooks/useTranslation";
import rootModalCss from "../../../RootModal.module.scss";
import css from "../../RegisterModal.module.scss";
import "react-phone-input-2/lib/style.css";

export default function ConfirmPhone({
  confirmPhone,
  setOneField,
  confirmPhoneAction,
  loading,
}) {
  const { t, locale } = useTranslation();
  const defaultCountry = locale === "en" ? "ru" : locale;

  const countryLocal = (lang) => {
    switch (lang) {
      case "ru":
        return ru;
      case "de":
        return de;
      default:
        return "";
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (!btnDisabled()) confirmPhoneAction({ phone: confirmPhone });
  };

  const btnDisabled = () => {
    if (loading || confirmPhone.length < 12) return true;
  };

  return (
    <div className={css.register__sms}>
      <div className={cx(css.register__sms_group, "flex row15")}>
        <PhoneInput
          value={confirmPhone}
          onChange={(data) => setOneField("confirmPhone", `+${data}`)}
          country={defaultCountry}
          preferredCountries={["ru", "de", "by", "ua", "kg", "kz"]}
          preserveOrder={["preferredCountries"]}
          localization={countryLocal(locale)}
          inputClass={"reg__phone_input"}
          containerClass={"reg__phone_container"}
          buttonClass={"reg__phone_button"}
          dropdownClass={"reg__phone_dropdown"}
        />
        <button
          className={cx(
            rootModalCss.submit__btn,
            btnDisabled() ? "btn__disabled" : ""
          )}
          onClick={handleSubmit}
        >
          {t("regModalSendBtn")}
        </button>
      </div>
    </div>
  );
}
