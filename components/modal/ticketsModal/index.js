import { useState } from "react";
import { connect } from "react-redux";
import {
  setSelectedTicket,
  useTicket,
  setTicketData,
} from "@store/actions/tickets";
import cx from "classnames/bind";
import TicketCanvas from "./ticketCanvas";
import useWindowSize from "@hooks/useWindowSize";
import { setPaymentRequestMessage } from "@store/actions/payment";
import css from "./TicketsModal.module.scss";
import { getPaymentType } from "@store/actions/payment";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { getInitialLocale } from "@translations/getInitialLocale";

const TicketsModal = ({
  hideModal,
  showModal,
  setSelectedTicket,
  selectedTicket,
  isAuth,
  paymentRequest,
  paymentStatus,
  useTicket,
  useTicketStatus,
  setPaymentRequestMessage,
  useTicketData,
  setTicketData,
  selectedTicketData,
}) => {
  const [ticketUsed, setTicketUsed] = useState(false);
  const screenSize = useWindowSize();
  const dispatch = useDispatch();

  const ticketHandleClick = (e, index) => {
    if (!e?.target?.classList?.contains(css.tickets__ticket_disabled)) {
      if (isAuth) {
        setSelectedTicket(index);
        showModal("PAYMENT_MODAL");
        dispatch(getPaymentType(selectedTicketData.id));
      } else {
        showModal("NOT_AUTHORIZED");
      }
    }
  };

  const handleClickTryAgain = () => {
    setSelectedTicket("");
    setPaymentRequestMessage("", "");
    setTicketData("");
  };

  const statusTicketUsed = (status) => {
    setTicketUsed(status);
  };

  const ticketsRender = () => {
    let items = [];
    let ticketCount;

    if (screenSize.width <= 768 && screenSize.width > 600) {
      ticketCount = 8;
    } else if (screenSize.width <= 600) {
      ticketCount = 3;
    } else {
      ticketCount = 9;
    }

    for (let i = 0; i < ticketCount; i++) {
      items.push(
        paymentRequest.status && selectedTicket === i ? (
          <TicketCanvas
            key={i}
            statusTicketUsed={statusTicketUsed}
            useTicket={useTicket}
            paymentStatus={paymentStatus}
            useTicketData={useTicketData}
          />
        ) : (
          <div
            key={i}
            onClick={(e) => ticketHandleClick(e, i)}
            className={cx(
              css.tickets__ticket,
              paymentRequest.status && selectedTicket !== i
                ? css.tickets__ticket_disabled
                : ""
            )}
          />
        )
      );
    }

    return items;
  };

  const [isVisibility, setVisibility] = useState("false");

  const handleHobbyClose = () => {
    setVisibility(!isVisibility);
  };

  const router = useRouter();

  const onClickChangeHobby = () => {
    hideModal();
    router.push(`/${getInitialLocale()}/profile`);
  };

  const HobbyChangeModal = () => {
    return (
      <div
        className={cx(
          isVisibility
            ? css.tickets__changeHobby_message
            : css.tickets__changeHobby_close
        )}
      >
        <span onClick={handleHobbyClose}></span>
        Не понравился приз? <a onClick={onClickChangeHobby}>Установите</a> более
        релевантные хобби
      </div>
    );
  };

  const ticketsSuccessRender = (type) => {
    if (selectedTicket !== "" && ticketUsed) {
      if (type === "btn") {
        return (
          <div className={cx(css.tickets__btn, "flex flex__x_center")}>
            <button onClick={handleClickTryAgain}>Попробовать еще раз</button>
          </div>
        );
      } else {
        return (
          <div className={css.tickets__modal_messages}>
            <div className={css.tickets__success_message}>
              <p>Информация о выигрыше отправлена на вашу электронную почту</p>
            </div>
            <HobbyChangeModal />
          </div>
        );
      }
    }
  };

  const handleCloseModal = () => {
    if (useTicketStatus) {
      setSelectedTicket("");
      setPaymentRequestMessage("", "");
      setTicketData("");
    }
    hideModal();
  };

  return (
    <div className={css.tickets__container}>
      <div className={css.tickets}>
        <div className={css.tickets__mobClose}>
          <div className={css.tickets__header_close} onClick={handleCloseModal}>
            <img src="/img/icons/closeModal.svg" alt="close" />
          </div>
        </div>
        <div className={cx(css.tickets__header, "flex flex__x_end")}>
          <div className={css.tickets__header_close} onClick={handleCloseModal}>
            <img src="/img/icons/closeModal.svg" alt="close" />
          </div>
        </div>
        <div className={css.tickets__content}>
          <div className={css.tickets__title}>
            <div className={css.tickets__title_layer}>
              <span>
                {!selectedTicket ? "Выберите билет" : "Сотрите билет"}
              </span>
            </div>
          </div>
          {ticketsSuccessRender()}
          <div className={css.tickets__wrap}>{ticketsRender()}</div>
          {ticketsSuccessRender("btn")}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  selectedTicket: state.tickets.selectedTicket,
  selectedTicketData: state.tickets.selectedTicketData,
  useTicketStatus: state.tickets.useTicketStatus,
  useTicketData: state.tickets.useTicketData,
  paymentRequest: state.payment.paymentRequest,
  paymentStatus: state.payment.paymentStatus,
  isAuth: state.auth.isAuth,
});

export default connect(mapStateToProps, {
  setSelectedTicket,
  useTicket,
  setPaymentRequestMessage,
  setTicketData,
})(TicketsModal);
