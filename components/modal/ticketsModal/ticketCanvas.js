import {useRef, useEffect, useState} from 'react'
import {isMobile} from 'react-device-detect';
import css from "@components/modal/ticketsModal/TicketsModal.module.scss";

const TicketCanvas = ({statusTicketUsed, useTicket, paymentStatus, useTicketData}) => {
  const {order_item_id} = paymentStatus;
  const canvasRef = useRef(null)
  const [hideCanvas, setHideCanvas] = useState(false)
  const {discount, name} = useTicketData;

  useEffect(() => {
    if(!hideCanvas) {
      const url = '/img/pages/home/ticket.jpg';
      let canvas = canvasRef.current
      let ctx = canvas.getContext('2d');
      let img = new Image();
      let transPercent = 0;
      let request = false;

      img.src = url;
      img.onload = () => {
        const width = Math.min(500, img.width);
        const height = img.height * (width / img.width);

        canvas.width = width;
        canvas.height = height;
        ctx.drawImage(img, 0, 0, width, height);
      };

      let isPress = false;
      let old = null;

      const percentTransparent = () => {
        let imageData = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
        let trans = 0;
        for (let i = 0; i < imageData.data.length; i += 4) {
          if (imageData.data[3 + i] === 0) {
            trans++
          }
        }

        transPercent = trans / (imageData.data.length / 4) * 100;
        if(transPercent >= 80) {
          statusTicketUsed(true);
        }
      }

      const getTouchPos = (canvasDom, touchEvent) => {
        const rect = canvasDom.getBoundingClientRect();
        return {
          x: touchEvent.touches[0].clientX - rect.left,
          y: touchEvent.touches[0].clientY - rect.top
        };
      }

      const checkIsPress = (e) => {
        e.preventDefault()
        if(isMobile) {
          old = getTouchPos(canvas, e)
        } else {
          old = {x: e.offsetX, y: e.offsetY}
        }

        isPress = true;
      }

      const erasingProcess = (e) => {
        e.preventDefault()
        if (isPress) {
          let x;
          let y;

          if(!request) {
            request = true;
            useTicket(order_item_id)
          }

          if(transPercent < 80) {
            if(isMobile) {
              x = getTouchPos(canvas, e).x;
              y = getTouchPos(canvas, e).y;
            } else {
              x = e.offsetX;
              y = e.offsetY;
            }

            ctx.globalCompositeOperation = 'destination-out';

            ctx.beginPath();
            ctx.arc(x, y, 20, 0, 2 * Math.PI);
            ctx.fill();

            ctx.lineWidth = 20;
            ctx.beginPath();
            ctx.moveTo(old.x, old.y);
            ctx.lineTo(x, y);
            ctx.stroke();

            old = {x: x, y: y};
            requestAnimationFrame(percentTransparent);
          }
          if(transPercent > 65) {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            setHideCanvas(true)
          }
        }
      }


      if(isMobile) {
        canvas.addEventListener("touchstart", checkIsPress, false);
        canvas.addEventListener("touchmove", erasingProcess, false);
        canvas.addEventListener('touchend', e => {
          e.preventDefault()
          isPress = false
        });
        canvas.addEventListener("touchcancel", e => e.preventDefault())
      } else {
        canvas.addEventListener('mousedown', checkIsPress, false);
        canvas.addEventListener('mousemove', erasingProcess, false);
        canvas.addEventListener('mouseup', () => isPress = false);
      }
    }
  }, [])


  return (
      <>
        <div className={css.tickets__canvas_wrap}>
          <div className={css.tickets__winning}>
            <p className={css.tickets__winning_title}>ПОЗДРАВЛЯЕМ!</p>
            {
              discount &&
              <div className={css.tickets__winning_discountWrap}>
                <p className={css.tickets__winning_discount}>Скидка</p>
                <p className={css.tickets__winning_result}>{discount}%</p>
              </div>
            }
            {
              name &&
              <div className={css.tickets__winning_name}>
                <p>на {name}</p>
              </div>
            }
          </div>
          {
            !hideCanvas && <canvas ref={canvasRef}/>
          }
        </div>
      </>
  )
}

export default TicketCanvas;