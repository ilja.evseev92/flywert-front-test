// import { useState } from "react";
import DatePicker, { registerLocale } from "react-datepicker";
import ru from "date-fns/locale/ru";
import InputMask from "react-input-mask";
import "react-datepicker/dist/react-datepicker.css";
import cx from "classnames/bind";
import css from "@styles/pages/Partner.module.scss";
registerLocale("ru", ru);

import { getMonth, getYear } from "date-fns";

const customHeader = ({ date, changeMonth, changeYear }) => {
  var months = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
  ];

  const years = new Array(5)
    .fill(null)
    .map((_, i) => new Date().getFullYear() + i);

  const handleYearChange = ({ target: { value } }) => changeYear(value);
  // const handleMonthChange = ({ target: { value } }) => changeMonth(value);

  return (
    <div>
      {/* <select onChange={handleMonthChange} value={getMonth(date)}>
        {months.map(({ value, label }) => (
          <option value={value} key={value}>
            {label}
          </option>
        ))}
      </select> */}

      <select
        value={months[getMonth(date)]}
        onChange={({ target: { value } }) => changeMonth(months.indexOf(value))}
      >
        {months.map((option) => (
          <option key={option} value={option}>
            {option}
          </option>
        ))}
      </select>

      <select onChange={handleYearChange} value={getYear(date)}>
        {years.map((year) => (
          <option value={year} key={year}>
            {year}
          </option>
        ))}
      </select>
    </div>
  );
};

const Сalendar = ({ startDate, setStartDate, finishDate, setFinishDate }) => {
  /*const [startDate, setStartDate] = useState(new Date());
  const [finishDate, setFinishDate] = useState(
    new Date().setMonth(new Date().getMonth() + 3)
  );*/
  /*console.log('startDate', startDate.toLocaleDateString('en-US'))
  console.log('finishDate', finishDate.toLocaleDateString('en-US'))*/

  return (
    <div className={cx(css.partner__ticketsDate)}>
      <div className={cx(css.partner__dateWrapper)}>
        <DatePicker
          selected={startDate}
          onChange={(date) => setStartDate(date)}
          renderCustomHeader={(props) => customHeader({ ...props })}
          dateFormat="dd/MM/yyyy"
          locale="ru"
          placeholderText="От"
          minDate={new Date()}
          customInput={<InputMask mask="99/99/9999" />}
        />
      </div>
      <div className={cx(css.partner__dateWrapper)}>
        <DatePicker
          selected={finishDate}
          onChange={(date) => setFinishDate(date)}
          renderCustomHeader={(props) => customHeader({ ...props })}
          dateFormat="dd/MM/yyyy"
          locale="ru"
          placeholderText="До"
          minDate={new Date()}
          customInput={<InputMask mask="99/99/9999" />}
        />
      </div>
    </div>
  );
};

export default Сalendar;
