import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import cx from "classnames/bind";
import css from "@styles/pages/Partner.module.scss";
import { useState } from "react";
import Select, { components } from "react-select";
import Сalendar from "./calendar";
import PublicOfferContract from "./publicOfferContract";
import { token, tokenLocal } from "@services/token";
import Cookies from "js-cookie";

const GeneralPartnerForm = ({
  setChosenDiscount,
  setToDate,
  setPartnerContacts,
  setDiscountObject,
  ...props
}) => {
  const [startDate, setStartDate] = useState(new Date());
  const [finishDate, setFinishDate] = useState(
    new Date().setMonth(new Date().getMonth() + 3)
  );

  const [selectedOption, setSelectedOption] = useState([]);
  const [selectedGender, setSelectedGender] = useState({
    value: "choose_gender",
    label: "Выберите аудиторию",
  });
  const [selectedDiscount, setSelectedDiscount] = useState({
    value: "5",
    label: "5%",
  });

  const activityOption = [
    { id: 1, value: "sport", label: "Спорт" },
    { id: 2, value: "fitness", label: "Фитнес" },
    { id: 3, value: "yoga_pilates", label: "Йога, Пилатес, Женские практики" },
    { id: 4, value: "healthy_lifestyle", label: "Здоровый образ жизни" },
    { id: 5, value: "medicine", label: "Медицина" },
    { id: 6, value: "relaxation", label: "Отдых" },
    { id: 7, value: "spa", label: "Спа" },
    { id: 8, value: "beauty_saloon", label: "Салон красоты" },
    { id: 9, value: "psychology", label: "Психология" },
    { id: 10, value: "business", label: "Бизнес" },
    { id: 11, value: "shopping", label: "Шоппинг" },
    {
      id: 12,
      value: "jurisprudence",
      label: "Юриспруденция или юридические услуги",
    },
    { id: 13, value: "travels", label: "Путешествия" },
    { id: 14, value: "education", label: "Образование и новые знания" },
    { id: 15, value: "languages", label: "Иностранные языки" },
    { id: 16, value: "culture", label: "Культура" },
    { id: 17, value: "beauty", label: "Красота и уход за собой" },
    { id: 18, value: "design", label: "Дизайн" },
    {
      id: 19,
      value: "healthy_nutrition",
      label: "Правильное питание (Тренер)",
    },
    {
      id: 20,
      value: "home_fitness",
      label: "Домашние тренировки или Домашний фитнес",
    },
    { id: 21, value: "online_shopping", label: "Онлайн шоппинг" },
    { id: 22, value: "computer_games", label: "Компьютерные игры" },
    { id: 23, value: "social_networks", label: "Социальные сети" },
    { id: 24, value: "restaurants_food", label: "Рестораны, еда" },
    { id: 25, value: "books", label: "Книги" },
    { id: 26, value: "electronics_phones", label: "Электроника, телефоны" },
    { id: 27, value: "tourism", label: "Туризм" },
    { id: 28, value: "visa", label: "Виза, переезд за границу" },
    { id: 29, value: "flowers", label: "Цветы" },
    { id: 30, value: "family_holidays", label: "Отдых с семьей" },
    { id: 31, value: "romantic_trip", label: "Романтическое путешествие" },
    {
      id: 32,
      value: "construction_materials",
      label: "Строительные материалы",
    },
    {
      id: 33,
      value: "extreme",
      label: "Экстремальные виды спорта (Парашют, квест)",
    },
    { id: 34, value: "art", label: "Искусство" },
    { id: 35, value: "photos", label: "Фотографии, фотосессия" },
    { id: 36, value: "sport_games", label: "Спортивные игры" },
    { id: 37, value: "mind_games", label: "Интеллектуальные игры" },
    { id: 38, value: "kids_games", label: "Детские игры" },
    { id: 39, value: "children_goods", label: "Товары для детей" },
    { id: 40, value: "children_shopping", label: "Детский шоппинг" },
    { id: 41, value: "music", label: "Музыка" },
    { id: 42, value: "fashion", label: "Мода" },
  ];
  const genderOption = [
    { value: "male", label: "Мужская" },
    { value: "female", label: "Женская" },
    { value: "general", label: "Для всех" },
  ];
  const discountOption = [
    { value: 5, label: "5%" },
    { value: 10, label: "10%" },
    { value: 15, label: "15%" },
    { value: 20, label: "20%" },
    { value: 25, label: "25%" },
    { value: 30, label: "30%" },
    { value: 35, label: "35%" },
    { value: 40, label: "40%" },
    { value: 45, label: "45%" },
    { value: 50, label: "50%" },
    { value: 55, label: "55%" },
    { value: 60, label: "60%" },
    { value: 65, label: "65%" },
    { value: 70, label: "70%" },
    { value: 75, label: "75%" },
    { value: 80, label: "80%" },
    { value: 85, label: "85%" },
    { value: 90, label: "90%" },
    { value: 95, label: "95%" },
    { value: 100, label: "100%" },
  ];

  const MultiValueLabel = (props) => {
    return <components.MultiValueLabel {...props} />;
  };

  const handleLocaleChange = (data) => {
    const catArr = [];
    data.map((item) => {
      catArr.push(item.id);
    });
    setSelectedOption(catArr);
  };

  const formik = useFormik({
    initialValues: {
      company_name: "",
      tax_number: "",
      email: "",
      phone: "",
      site_url: "",
      promo: "",
      address: "",
      activity: [],
      discountTarget: "",
      gender: "",
      lat: Cookies.get("lat") || "52.784603",
      lng: Cookies.get("lng") || "20.448843",
      audience: "",
      //discount: "",
      //tickets_count: "",
      tickets_date_start: "",
      tickets_date_end: "",
      //audience: "",
      //lang: "",
      //logo: "",
      checkboxes: "",
    },
    validationSchema: Yup.object({
      company_name: Yup.string().required("Required"),
      tax_number: Yup.string().required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
      phone: Yup.number().required("Required"),
      // site_url: Yup.string().required("Required"),
      // promo: Yup.string().required("Required"),
      address: Yup.string().required("Required"),
      activity: Yup.array()
        .max(3)
        .of(
          Yup.object().shape({
            id: Yup.string().required(),
            value: Yup.string().required(),
            label: Yup.string().required(),
          })
        ), //!!!!!!!!!!
      discountTarget: Yup.string().required("Required"),
      //gender: Yup.string().required("Required"), //!!!!!!!!!!
      //discount: Yup.string().required("Required"), //!!!!!!!!!!
      //tickets_count: Yup.string().required("Required"), //!!!!!!!!!!
      //tickets_date_start: Yup.string().required("Required"),
      //.email("Invalid email address")
      //.required("Required"),
      //tickets_date_end: Yup.string().required("Required"),
      //audience: Yup.string().required("Required"), //!!!!!!!!!!
      //lang: Yup.string().required("Required"), //!!!!!!!!!!
      //logo: Yup.string().required("Required"), //!!!!!!!!!!
      checkboxes: Yup.string().required("Required"),
    }),
    onSubmit: async () => {
      const newPartner = {
        name: formik.values.company_name,
        tax_number: formik.values.tax_number,
        email: formik.values.email,
        phone: formik.values.phone,
        checkboxes: formik.values.checkboxes,
        //"site": formik.values.site_url,
        //"services": [1, 2, 3],
        branches: [
          {
            id: 1,
            lat: formik.values.lat,
            lng: formik.values.lng,
            categories: selectedOption,
            gender: [selectedGender.value],
            discountTarget: formik.values.discountTarget,
            discount: selectedDiscount.value,
            valid_from: startDate.toLocaleDateString("en-US"),
            valid_to: finishDate.toLocaleDateString("en-US"),
          },
        ],
      };
      try {
        const response = await fetch(
          "https://api.flywert-dev.ru/api/v1/partners",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "X-Localization": "en",
              authorization: `Bearer ${Cookies.getJSON("access_token")}`,
              //Authorization: 'Bearer ' + token,
              Accept: "application/json",
            },
            body: JSON.stringify(newPartner),
          }
        );
        const item = await response.json();

        console.log(item);
        console.log(newPartner);
      } catch (e) {
        console.error(e);
      }
    },
  });

  setChosenDiscount(selectedDiscount.label);

  switch (typeof finishDate) {
    case "number":
      setToDate("01/01/2021");
      break;
    case "object":
      setToDate(finishDate.toLocaleDateString("ru-RU"));
      break;
  }

  setPartnerContacts(formik.values.phone);
  setDiscountObject(formik.values.discountTarget);

  const handleGenderChange = (data) => {
    setSelectedGender({ value: data.value, label: data.label });
  };

  const handleDiscountChange = (data) => {
    setSelectedDiscount({ value: data.value, label: data.label });
  };

  function ServiceInclude() {
    return (
      <div className={css.tooltip}>
        <p className={css.tooltip__desc}>
          Здесь описание этого пункта - текст мы напишем
        </p>
      </div>
    );
  }

  const RedDiscount = () => {
    return (
      <div className={cx(css.partner__discountDesc)}>
        <p>
          Вероятность прихода - <span>НИЗКАЯ</span>
        </p>
        <p>
          <span>Рекомендация:</span> увеличьте процент скидки
        </p>
      </div>
    );
  };
  const YellowDiscount = () => {
    return (
      <div className={cx(css.partner__discountDesc_yellow)}>
        <p>
          Вероятность прихода - <span>СРЕДНЯЯ</span>
        </p>
        {/*<p>
                    <span>Рекомендация:</span> увеличьте процент скидки
                </p>*/}
      </div>
    );
  };
  const GreenDiscount = () => {
    return (
      <div className={cx(css.partner__discountDesc_green)}>
        <p>
          Вероятность прихода - <span>ВЫСОКАЯ</span>
        </p>
        {/*<p>
                    <span>Рекомендация:</span> увеличьте процент скидки
                </p>*/}
      </div>
    );
  };

  let colorClass;
  if (selectedDiscount.value < 30) {
    colorClass = <RedDiscount />;
  }
  if (selectedDiscount.value >= 30 && selectedDiscount.value <= 60) {
    colorClass = <YellowDiscount />;
  }
  if (selectedDiscount.value > 60) {
    colorClass = <GreenDiscount />;
  }

  const [modalIsOpen, setIsOpen] = useState(false);
  function openModal() {
    setIsOpen(true);
  }

  return (
    <div className={css.partner__form}>
      <h3>Общие сведения и контакты</h3>
      <form onSubmit={formik.handleSubmit}>
        <label
          htmlFor="company_name"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Название вашей компании
        </label>
        <input
          id="company_name"
          name="company_name"
          type="text"
          autoComplete="off"
          placeholder="Название"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.company_name}
          className={
            formik.touched.company_name &&
            formik.errors.company_name &&
            "input__error"
          }
        />
        <label
          htmlFor="tax_number"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Налоговый номер
        </label>
        <input
          id="tax_number"
          name="tax_number"
          type="text"
          autoComplete="off"
          placeholder="000000000000000"
          maxLength="15"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.tax_number}
          className={
            formik.touched.tax_number &&
            formik.errors.tax_number &&
            "input__error"
          }
        />
        <label
          htmlFor="email"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Ваш E-mail
        </label>
        <input
          id="email"
          name="email"
          type="email"
          autoComplete="off"
          placeholder="email@mail.ru"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.email}
          className={
            formik.touched.email && formik.errors.email && "input__error"
          }
        />
        <label
          htmlFor="phone"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Номер вашего телефона
        </label>
        <input
          id="phone"
          name="phone"
          type="tel"
          placeholder="+7__________"
          maxLength="15"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.phone}
          className={
            formik.touched.phone && formik.errors.phone && "input__error"
          }
        />
        <label htmlFor="site_url" className={cx(css.partner__label)}>
          Сайт
        </label>
        <input
          id="site_url"
          name="site_url"
          type="url"
          autoComplete="off"
          placeholder="Ссылка на сайт"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.site_url}
          className={
            formik.touched.site_url && formik.errors.site_url && "input__error"
          }
        />
        <label htmlFor="promo" className={cx(css.partner__label)}>
          Промокод
        </label>
        <input
          id="promo"
          name="promo"
          type="text"
          autoComplete="off"
          placeholder="Промокод"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.promo}
          className={
            formik.touched.promo && formik.errors.promo && "input__error"
          }
        />
        <div className={css.partner__filialForm}>
          <div className={css.partner__infoHeader}>
            <h3>Офис/Филиал №1</h3>
            <div className={css.partner__include_icon}>
              <span className={cx(css.partner__info_icon)}></span>
              <ServiceInclude />
            </div>
          </div>
        </div>
        <label
          htmlFor="address"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Адрес филиала
        </label>
        <input
          id="address"
          name="address"
          type="text"
          autoComplete="off"
          placeholder="Россия, Москва, ул.Долгоруковская, 5"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.address}
          className={
            formik.touched.address && formik.errors.address && "input__error"
          }
        />
        <div className={css.partner__infoLabel}>
          <label
            htmlFor="activity"
            className={cx(css.partner__requiredField, css.partner__label)}
          >
            Профиль филиала
          </label>
          <div className={css.partner__include_icon}>
            <span className={cx(css.partner__info_icon)}></span>
            <ServiceInclude />
          </div>
        </div>
        <Select
          inputId="activity"
          name="activity"
          options={activityOption}
          // value={selectedOption}
          onChange={handleLocaleChange}
          isSearchable={false}
          placeholder="Выберите направление"
          closeMenuOnSelect={false}
          // components={{ MultiValueLabel }}
          // defaultValue={selectedOption}
          isMulti
          className={cx(
            css.partner__select,
            formik.touched.address && formik.errors.address && "input__error"
          )}
        />
        <label
          htmlFor="genderSelect"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Целевая аудитория
        </label>
        <Select
          inputId="genderSelect"
          options={genderOption}
          value={selectedGender}
          onChange={handleGenderChange}
          isSearchable={false}
          className={cx(
            css.partner__select,
            formik.touched.address && formik.errors.address && "input__error"
          )}
        />
        <label
          htmlFor="discountTarget"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Объект скидки (товар/услуга)
        </label>
        <input
          id="discountTarget"
          name="discountTarget"
          type="text"
          autoComplete="off"
          placeholder="Выберите название услуги"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.discountTarget}
          className={
            formik.touched.discountTarget &&
            formik.errors.discountTarget &&
            "input__error"
          }
        />
        <div className={css.partner__infoLabel}>
          <label htmlFor="discountSelect" className={cx(css.partner__label)}>
            Процент скидки
          </label>
          <div className={css.partner__include_icon}>
            <span className={cx(css.partner__info_icon)}></span>
            <ServiceInclude />
          </div>
        </div>
        <div className={cx(css.partner__discount)}>
          <Select
            inputId="discountSelect"
            options={discountOption}
            value={selectedDiscount}
            onChange={handleDiscountChange}
            isSearchable={false}
            className={css.partner__select}
          />

          {colorClass}
        </div>
        <div className={css.partner__infoLabel}>
          <label
            htmlFor="tickets_date_start"
            className={cx(css.partner__label)}
          >
            Срок действия билетов (минимум&nbsp;3&nbsp;месяца)
          </label>
          <div className={css.partner__include_icon}>
            <span className={cx(css.partner__info_icon)}></span>
            <ServiceInclude />
          </div>
        </div>
        <Сalendar
          setStartDate={setStartDate}
          startDate={startDate}
          setFinishDate={setFinishDate}
          finishDate={finishDate}
        />
        {/*<div className={(css.partner__ticket, css.partner__ticketMobile)}>
          <h3>Предварительный просмотр билета</h3>
          <div className={css.partner__ticketPreview}>
            <p className={css.partner__ticketPreview_title}>ПОЗДРАВЛЯЕМ!</p>

            <div className={css.partner__ticketPreview_discountWrap}>
              <p className={css.partner__ticketPreview_discount}>Скидка</p>
              <p className={css.partner__ticketPreview_result}>30%</p>
              <p className={css.partner__ticketPreview_name}>
                на 'этот текст меняется'
              </p>
            </div>

            <div className={css.partner__ticketPreview_infoWrap}>
              <div className={css.partner__ticketPreview_contacts}>
                <p className={css.partner__ticketPreview_contactText}>
                  Для получения выигрыша обращайтесь:
                </p>
                <p className={css.partner__ticketPreview_contact}>
                  "Какие-то контакты"
                </p>
              </div>
              <p>
                До:{" "}
                <span className={css.partner__ticketPreview_date}>Date</span>
              </p>
            </div>
          </div>
        </div>*/}
        <label className={css.partner__checkbox}>
          <input
            type="checkbox"
            name="checkboxes"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.checkboxes}
            className={cx(
              css.partner__checkboxInput,
              formik.touched.checkboxes &&
                formik.errors.checkboxes &&
                "input__error"
            )}
          />
          <span
            className={cx(
              css.partner__requiredField,
              css.partner__checkboxSpan
            )}
          >
            Согласен с <PublicOfferContract />
          </span>
        </label>
        <button
          className={cx(
            css.partner__submitBtn,
            props.loading ||
              formik.errors.company_name ||
              formik.errors.tax_number ||
              formik.errors.email ||
              formik.errors.phone ||
              formik.errors.address ||
              formik.errors.discountTarget ||
              formik.errors.checkboxes
              ? "btn__disabled"
              : ""
          )}
          type="submit"
        >
          Стать партнером
        </button>
      </form>
    </div>
  );
};

export default GeneralPartnerForm;
