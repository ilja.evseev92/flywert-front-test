import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import cx from "classnames/bind";
import css from "@styles/pages/Partner.module.scss";
import Сalendar from "./calendar";

const PartnerFilial = (props) => {
  const [selectedOption, setSelectedOption] = useState({
    value: "choose_option",
    label: "Салон красоты",
  });
  const [selectedGender, setSelectedGender] = useState({
    value: "choose_gender",
    label: "Выберите аудиторию",
  });
  const [selectedDiscount, setSelectedDiscount] = useState({
    value: "choose_discount",
    label: "5%",
  });

  const activityOption = [
    { value: "beauty_saloon", label: "Салон красоты" },
    { value: "barbershop", label: "Парикмахерская" },
    { value: "activity", label: "Деятельность" },
    { value: "cosmetology", label: "Косметология" },
  ];
  const genderOption = [
    { value: "male", label: "Мужская" },
    { value: "female", label: "Женская" },
    { value: "general", label: "Для всех" },
  ];
  const discountOption = [
    { value: "5%", label: "5%" },
    { value: "10%", label: "10%" },
    { value: "15%", label: "15%" },
    { value: "20%", label: "20%" },
    { value: "25%", label: "25%" },
    { value: "30%", label: "30%" },
    { value: "35%", label: "35%" },
    { value: "40%", label: "40%" },
    { value: "45%", label: "45%" },
    { value: "50%", label: "50%" },
    { value: "55%", label: "55%" },
    { value: "60%", label: "60%" },
    { value: "65%", label: "65%" },
    { value: "70", label: "70%" },
    { value: "75%", label: "75%" },
    { value: "80%", label: "80%" },
    { value: "85%", label: "85%" },
    { value: "90%", label: "90%" },
    { value: "95%", label: "95%" },
    { value: "100%", label: "100%" },
  ];
  const formik = useFormik({
    initialValues: {
      address: "",
      activity: "",
      gender: "",
      discount: "",
      tickets_count: "",
      tickets_date_start: "",
      tickets_date_end: "",
      audience: "",
      lang: "",
      logo: "",
      checkboxes: "",
    },
    validationSchema: Yup.object({
      address: Yup.string().required("Required"),
      activity: Yup.string().required("Required"),
      gender: Yup.string().required("Required"),
      discount: Yup.string().required("Required"),
      tickets_count: Yup.string().required("Required"),
      tickets_date_start: Yup.string()
        .email("Invalid email address")
        .required("Required"),
      tickets_date_end: Yup.string().required("Required"),
      audience: Yup.string().required("Required"),
      lang: Yup.string().required("Required"),
      logo: Yup.string().required("Required"),
      checkboxes: Yup.string().required("Required"),
    }),
    onSubmit: (values) => {
      console.log("PartnerForm", values);
    },
  });

  const handleLocaleChange = (data) => {
    setSelectedOption({ value: data.value, label: data.label });
  };

  const handleGenderChange = (data) => {
    setSelectedGender({ value: data.value, label: data.label });
  };

  const handleDiscountChange = (data) => {
    setSelectedDiscount({ value: data.value, label: data.label });
  };

  function ServiceInclude() {
    return (
      <div className={css.tooltip}>
        <p className={css.tooltip__desc}>
          Здесь описание этого пункта - текст мы напишем. А сейчас пока это
          просто текст текст текст текст текст текст текст текст текст текст
          текст текст текст текст текст текст текст текст текст текст текст
          текст текст текст текст текст текст текст текст текст текст текст
          текст текст текст текст текст текст текст текст текст текст текст
          текст текст
        </p>
      </div>
    );
  }

  return (
    <div className={css.partner__filialForm}>
      <div className={css.partner__infoHeader}>
        <h3>Офис/Филиал №1</h3>
        <div className={css.partner__include_icon}>
          <span className={cx(css.partner__info_icon)}></span>
          <ServiceInclude />
        </div>
      </div>

      <form onSubmit={formik.handleSubmit}>
        <label
          for="address"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Адрес филиала
        </label>
        <input
          id="address"
          name="address"
          type="text"
          autoComplete="off"
          placeholder="Россия, Москва, ул.Долгоруковская, 5"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.address}
          className={
            formik.touched.address && formik.errors.address && "input__error"
          }
        />

        <div className={css.partner__infoLabel}>
          <label
            htmlFor="activitySelect"
            className={cx(css.partner__requiredField, css.partner__label)}
          >
            Выберите направление/профиль филиала
          </label>
          <div className={css.partner__include_icon}>
            <span className={cx(css.partner__info_icon)}></span>
            <ServiceInclude />
          </div>
        </div>

        <Select
          inputId="activitySelect"
          options={activityOption}
          value={selectedOption}
          onChange={handleLocaleChange}
          isSearchable={false}
          className={cx(
            css.partner__select,
            formik.touched.address && formik.errors.address && "input__error"
          )}
        />

        <label
          htmlFor="genderSelect"
          className={cx(css.partner__requiredField, css.partner__label)}
        >
          Целевая аудитория
        </label>
        <Select
          inputId="genderSelect"
          options={genderOption}
          value={selectedGender}
          onChange={handleGenderChange}
          isSearchable={false}
          className={cx(
            css.partner__select,
            formik.touched.address && formik.errors.address && "input__error"
          )}
        />

        <div className={css.partner__infoLabel}>
          <label for="discountSelect" className={cx(css.partner__label)}>
            Процент скидки
          </label>
          <div className={css.partner__include_icon}>
            <span className={cx(css.partner__info_icon)}></span>
            <ServiceInclude />
          </div>
        </div>

        <div className={cx(css.partner__discount)}>
          <Select
            inputId="discountSelect"
            options={discountOption}
            value={selectedDiscount}
            onChange={handleDiscountChange}
            isSearchable={false}
            className={css.partner__select}
          />
          <div className={cx(css.partner__discountDesc)}>
            <p>
              Вероятность прихода - <span>низкая</span>
            </p>
            <p>
              <span>Рекомендация:</span> увеличьте процент скидки
            </p>
          </div>
        </div>

        <div className={css.partner__infoLabel}>
          <label
            htmlFor="tickets_date_start"
            className={cx(css.partner__label)}
          >
            Срок действия билетов (минимум&nbsp;3&nbsp;месяца)
          </label>
          <div className={css.partner__include_icon}>
            <span className={cx(css.partner__info_icon)}></span>
            <ServiceInclude />
          </div>
        </div>

        <Сalendar />

        <div className={(css.partner__ticket, css.partner__ticketMobile)}>
          <h3>Предварительный просмотр билета</h3>
          <div className={css.partner__ticketPreview}></div>
        </div>

        <label className={css.partner__checkbox}>
          <input
            type="checkbox"
            name="checkboxes"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.checkboxes}
            className={cx(
              css.partner__checkboxInput,
              formik.touched.checkboxes &&
                formik.errors.checkboxes &&
                "input__error"
            )}
          />
          <span
            className={cx(
              css.partner__requiredField,
              css.partner__checkboxSpan
            )}
          >
            Согласен с <a href="#">договором</a>
          </span>
        </label>

        <button
          className={cx(
            css.partner__submitBtn,
            props.loading ||
              formik.errors.company_name ||
              formik.errors.tax_number ||
              formik.errors.email ||
              formik.errors.phone ||
              formik.errors.checkboxes
              ? "btn__disabled"
              : ""
          )}
          type="submit"
        >
          Стать партнером
        </button>
      </form>
    </div>
  );
};

export default PartnerFilial;
