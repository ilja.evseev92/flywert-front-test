import cx from 'classnames/bind';
import css from './BtnWithIcon.module.scss'

const BtnWithIcon = props => {
  const {title, iconUrl} = props
  return (
      <div className={cx(css.btnWithIcon, "flex__center")}>
        {iconUrl ? <img src={iconUrl} alt="icon"/> : ''}
        <span>{title}</span>
      </div>
  )
}

export default BtnWithIcon;