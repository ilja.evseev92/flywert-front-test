import ProfileConsole from './console'
import ProfileInfo from "./profileInfo";
import ProfileOrders from './profileOrders'
import {contentComponents} from "../utils"
import css from './ProfileContent.module.scss'

const ProfileContent = props => {
  const {nickname, first_name, last_name} = props.userData;
  const {activeContent, loading, dispatch, success} = props;

  const renderProfileContent = () => {
    switch (activeContent) {
      case contentComponents[0].type:
        return (
            <ProfileConsole
                nickName={nickname}
                first_name={first_name}
                last_name={last_name}
            />
        )
      case contentComponents[1].type:
        return (
            <ProfileInfo
                loading={loading}
                success={success}
                userData={props.userData}
                dispatch={dispatch}
            />
        )
      case contentComponents[2].type:
        return (
            <ProfileOrders dispatch={dispatch} />
        )
      default:
        return
    }
  }
  return (
      <div className={css.profileContent}>
        {renderProfileContent()}
      </div>
  )
}

export default ProfileContent;