import ProfileAvatar from './profileAvatar'
import BtnWithIcon from '../btn/btnWithIcon'
import ProfileNav from './profileNavigation'
import css from './ProfileSidebar.module.scss'

const ProfileSidebar = props => {
  const {avatar} = props.userData;
  const {dispatch, setActiveContent, activeContent} = props;

  return (
      <div className={css.profileSidebar}>
        <ProfileAvatar avatar={avatar} dispatch={dispatch} />
        <div className={css.profileSidebar__btnGroup}>
          <BtnWithIcon title="Мои друзья" iconUrl="/img/pages/profile/myRriends.svg"/>
        </div>
        <div className={css.profileSidebar__nav}>
          <ProfileNav setActiveContent={setActiveContent} activeContent={activeContent} dispatch={dispatch}/>
        </div>
      </div>
  )
}

export default ProfileSidebar;