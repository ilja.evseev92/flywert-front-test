import {contentComponents} from '../../utils'
import css from './ProfileNav.module.scss'
import {LogoutAction} from '@store/actions/auth'


const ProfileNav = props => {
  const {activeContent, setActiveContent, dispatch} = props

  const renderNav = () => {
    return (
        <nav className={css.profileNav}>
          <ul>
            {contentComponents.map(item => (
                <li
                    key={item.type}
                    className={item.type === activeContent ? css.active : ''}
                    onClick={() => {
                      if(item.type === 'LOGOUT') {
                        LogoutAction()(dispatch)
                      } else {
                        setActiveContent(item.type)
                      }
                    }}
                >
                  {item.title}
                </li>
            ))}
          </ul>
        </nav>
    )
  }

  return renderNav()
}

export default ProfileNav;