export const contentComponents = [
  {type: 'PROFILE_HOME', title: 'Консоль'},
  {type: 'PROFILE', title: 'Профиль'},
  {type: 'PROFILE_ORDER', title: 'Заказы'},
  {type: 'ACCOUNT_TRANSACTION', title: 'История транзакций счета'},
  {type: 'BALANCE', title: 'Пополнить баланс'},
  {type: 'LOGOUT', title: 'Выйти'}
]