import React from "react";

function ButtonRightArrow() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="7"
      height="10"
      fill="none"
      viewBox="0 0 7 10"
    >
      <path
        fill="#fff"
        d="M.884 9.287L5.6 5 .884.713l.65-.59L6.9 5 1.534 9.878l-.65-.59z"
      ></path>
    </svg>
  );
}

export default ButtonRightArrow;
