import { connect } from 'react-redux';
import { setSelectedTicketData } from '@store/actions/tickets';
import { showModal } from '@store/actions/modal';
import Product from './product';
import css from './Products.module.scss';
import cx from 'classnames/bind';



const Products = (props) => {
  const { fullScreen = false, products } = props;

  /*  let requestData;
  if (!isAuth) {
    requestData = {
      filters: {
        lat: "50.595414",
        lng: "36.587268",
        range: 5,
      }
    }
  } else {
    requestData = {
      filters: {
        lat: "50.595414",
        lng: "36.587268",
        range: 5,
        gender: 'male',
        categories: [1,2,3]
      }
    }
  }

  function getProducts(method, url) {
    const headers = {
      'Content-Type': 'application/json',
      'X-Localization': 'en',
      //'authorization': `Bearer ${Cookies.getJSON('access_token')}`,
      Authorization: 'Bearer ' + token,
      Accept: 'application/json',
    }
    return fetch(url, {
      method: method,
      headers: headers,
      body: JSON.stringify(requestData),
    }).then(response => {
      return response.json()
    })
  }

  useEffect(() => {
    getProducts('POST', urlGetProducts)
        .then(item => setUserProducts(item.data))
        .catch(err => console.log(err))
  },[])*/

  return (
    <div className={cx(fullScreen && css.fullScreen, css.products)}>
      {products &&
      products.map((item) => (
          <Product
            product={item}
            key={item.id}
            showModal={props.showModal}
            setSelectedTicketData={props.setSelectedTicketData}
            getPaymentType={props.getPaymentType}
          />
        ))}
    </div>
  );
};


export default connect(null, { showModal, setSelectedTicketData })(Products);
