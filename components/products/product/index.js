import ButtonRightArrow from "../ButtonRightArrow";
import css from "../Products.module.scss";
import PrimaryBtn from "@components/btn/primaryBtn";
import cx from "classnames/bind";



export default function Product({ product, showModal, setSelectedTicketData }) {
  const {
    id,
    small_text,
    currency,
    image,
    is_new,
    name,
    price,
    product_includes,
    slug,

  } = product;

  function ServiceInclude() {
    return (
      <div className={css.tooltip}>
        <h2 className={css.tooltip__header}>В данную услугу входит:</h2>
        <ul className={css.tooltip__list}>
          {product_includes &&
            product_includes.map((item, index) => (
              <li className={css.tooltip__item} key={index}>
                {item.tooltip_desc}
              </li>
            ))}
        </ul>
        <PrimaryBtn
          name="Подробнее"
          className={cx("btn-primary", css.btnInclude)}
        />
      </div>
    );
  }

  const handlePayClick = () => {
    setSelectedTicketData({ name, price, currency, id });
    showModal("TICKETS_MODAL");
  };

  // Вывод дефолтных миниатюр продуктов;
  function srcImgDeafault() {
    let srcImg = [];
    for (let i = 0; i < 14; ++i) {
      srcImg.push(`/img/pages/base-products/${i - 1}.png`);
    }
    return srcImg;
  }

  return (
    <div className={css.product} key={id}>
      <div className={css.product__image_wrap}>
        <img
          className={css.product__icon}
          src={[product.image] != "" ? image : srcImgDeafault()[product.id]}
          alt={name}
          onClick={handlePayClick}
        />
      </div>

      <div className={cx(!is_new && css.product__noNew, css.product__desc)}>
        <div className={css.product__header} onClick={handlePayClick}>
          {name}
        </div>
        <span>{small_text}</span>
      </div>

      <div className={css.product__right}>
        <div className={css.product__info}>
          {is_new && <div className={css.product__new} />}
          <div className={css.product__include_icon}>
            <ServiceInclude />
          </div>
        </div>
        <button className={css.product__pay} onClick={handlePayClick}>
          {price} {currency} <ButtonRightArrow />
        </button>
      </div>
    </div>
  );
}
