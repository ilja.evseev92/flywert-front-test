import React from "react";

export default function Star() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="15"
      height="15"
      fill="none"
      viewBox="0 0 15 15"
    >
      <path
        fill="#F2C94C"
        d="M10.371 9.185L12.187 15 7.5 11.396 2.812 15 4.63 9.185 0 5.625h5.742L7.5 0l1.758 5.625H15l-4.629 3.56z"
      ></path>
    </svg>
  );
}
