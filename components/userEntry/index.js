import styles from './UserEntry.module.scss';

export default function UserEntry(props) {
    return (
        <div className={styles.userEntry}>
            {
                props.access_token ?
                    <div>
                        <button className={styles.userEntry__btn} onClick={() => props.LogoutAction()}>Выход</button>
                    </div> :
                    <div>
                        <button className={styles.userEntry__btn}
                                onClick={() => props.setModalStatus('modalReg')}>Регистрация
                        </button>
                        <button className={styles.userEntry__btn}
                                onClick={() => props.setModalStatus('modalLogin')}>Вход
                        </button>
                    </div>
            }

        </div>
    );
}
