import { useEffect, useState } from 'react';
import Winner from "./Winner";
import PrimaryBtn from "../../components/btn/primaryBtn";
import cx from "classnames/bind";
import css from "./Winners.module.scss";

const winnersDefault = {
  title: "Победители недели",
  elems: [
    { title: "Андрей Софронов", avatarUrl: "url" },
    { title: "Иван Иванов", avatarUrl: "url" },
    { title: "Аркадий Аркадиевич", avatarUrl: "url" },
  ],
};

/*function ShowWinners() {
  return winnersList.elems.map((winner, i) => {
    const [name, surname] = winner.title.split(" ");
    return <Winner key={i} name={name} surname={surname} />;
  });
}*/

export default function Winners({winners}) {

    const winnersActualList = (winners === []) ? winners : winnersDefault;
    //console.log(winnersActualList)

    function ShowWinners() {
        return winnersActualList.elems.map((winner, i) => {
            const [name, surname] = winner.title.split(" ");
            return <Winner key={i} name={name} surname={surname} />;
        });
    }
    return (
        <div className={css.winners}>
          <h2 className={css.winnersList__header}>{winnersActualList.title}</h2>
          <div className={css.winnersList}>
            <ShowWinners />
          </div>
          <PrimaryBtn
            name="Больше победителей"
            className={cx(css.btn_winners, "btn-primary")}
          />
        </div>
      );
}
