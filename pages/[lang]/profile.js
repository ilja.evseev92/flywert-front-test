import React, {useEffect, useState} from 'react'
import cx from 'classnames/bind';
import Layout from '@components/layout'
import withAuth from '@hocs/withAuth'
import {contentComponents} from '@components/pages/profile/utils'
import {getUserData} from "@store/actions/user";
import BreadCrumbs from '@components/breadCrumbs'
import ProfileSidebar from '@components/pages/profile/sidebar'
import ProfileContent from '@components/pages/profile/content'
import css from '@styles/pages/Profile.module.scss';

const Profile = props => {
  const {dispatch, state} = props;
  const {userData} = state.user;
  const {loading, success} = state.helpers;
  const [activeContent, setActiveContent] = useState(contentComponents[0].type)

  useEffect(() => {
    getUserData()(dispatch)
  }, [])

  return (
      <Layout {...props.auth}>
        <div className={css.profilePage}>
          <BreadCrumbs breadCrumbActive="Мой профиль"/>
          <div className={cx(css.profilePage__container, 'flex')}>
            <ProfileSidebar
                userData={userData}
                dispatch={dispatch}
                setActiveContent={setActiveContent}
                activeContent={activeContent}
            />
            <ProfileContent
                userData={userData}
                dispatch={dispatch}
                activeContent={activeContent}
                loading={loading}
                success={success}
            />
          </div>
        </div>
      </Layout>
  )
}

export default withAuth(Profile)
