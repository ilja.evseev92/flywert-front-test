import cx from 'classnames/bind'
import Layout from "@components/layout";
import css from '@styles/pages/TextPage.module.scss'

const TextPage = () => {
  return (
      <Layout>
        <div className={cx(css.textPage, "flex flex-wrap")}>
          <div className={css.textPage__desc}>
            <h1>Поступление в ВУЗ Германия</h1>
            <div className={css.textPage__desc_inner}>
              <p>
                Европейское образование ценится по всему миру. С данным дипломом обычно берут на работу охотнее, чем с другими дипломами. Также высока
                и оплата труда. Вы все ещё сомневаетесь? Учиться в Германии, как минимум престижно, а практика в международных компаниях во время обучения даст вам старт для новых свершений!
              </p>
              <p>
                Вы самостоятельно выбираете Вузы, и изучаете условия поступления. Мы поможем вам оформить все необходимые документы и свяжемся с выбранным Вузом лично. Обсудим с вами все нюансы и приступим к вашему зачислению
                в Вуз. Обучение происходит на немецком языке.
              </p>
              <p>
                Вы самостоятельно выбираете Вузы, и изучаете условия поступления. Мы поможем вам оформить все необходимые документы и свяжемся с выбранным Вузом лично. Обсудим с вами все нюансы и приступим к вашему зачислению
                в Вуз. Обучение происходит на немецком языке.
              </p>
            </div>
          </div>
          <div className={css.textPage__image}>
            <h1 className={css.textPage__image_title}>Поступление в ВУЗ Германия</h1>
            <picture>
              <source srcSet="/img/pages/textPage/textPage.jpg" media="(max-width: 480px)" />
              <source srcSet="/img/pages/textPage/textPageMd.jpg" media="(max-width: 768px)" />
              <img src="/img/pages/textPage/textPage.jpg" alt="text page" />
            </picture>
          </div>
          <div className={css.textPage__listWrap}>
            <p className={css.textPage__subtitle}>От вас необходимы:</p>
            <ul>
              <li>Аттестат об окончании 11 классов;</li>
              <li>2 года обучения в Вузе и соответствующая справка;</li>
              <li>Хороший немецкий язык и сертификат подтверждающий знания.</li>
            </ul>
            <p className={css.textPage__listWrap_desc}>
              В случае выигрыша, вы сможете воспользоваться им в течение 12 месяцев. Передавать выигрыш третьим лицам запрещено. В случае отказа в визе
              по вашей вине, выигрыш обмену и возврату не принадлежит.
            </p>
          </div>
        </div>
      </Layout>
  )
}

export default TextPage;