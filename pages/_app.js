import {useEffect} from 'react';
import Auth from '@services/auth';
import {wrapper, initStore} from '@store/storeConfig';
import {AUTH_SET_ONE_FIELD} from '@store/constants/auth';
import '@styles/global.scss';


const MyApp = ({Component, pageProps, auth}) => {

  useEffect(() => {
    const store = initStore();
    store.dispatch({type: AUTH_SET_ONE_FIELD, key: 'isAuth', payload: !!auth.isAuthenticated})
  }, [auth.isAuthenticated])

  return (
      <Component {...pageProps} auth={auth}/>
  )
}

MyApp.getInitialProps = async ({Component, router, ctx, store}) => {
  let pageProps = {};

  const user = process.browser ? Auth.clientAuth() : Auth.serverAuth(ctx.req);
  const auth = {user, isAuthenticated: !!user};

  await ctx.store.dispatch({type: AUTH_SET_ONE_FIELD, key: 'isAuth', payload: !!user});

  if(Component.getInitialProps) pageProps = await Component.getInitialProps(ctx)

  return {pageProps, auth}
}


export default wrapper.withRedux(MyApp);

