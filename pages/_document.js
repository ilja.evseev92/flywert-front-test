import React from "react";
import Document, { Html, Head, Main, NextScript } from "next/document";

export default class extends Document {
  render() {
    return (
      <Html>
        <Head />
        <body>
        <div className="wrapper">
          <Main />
          <NextScript />
        </div>
        </body>
      </Html>
    );
  }
}
