import Http from "./http.init";
import Cookies from 'js-cookie'

class AuthService {
  constructor(entity, data = {}, isAuth) {
    this.data = data;
    this.entity = entity;
    this.isAuth = isAuth;
  }

  post() {
    return new Promise((resolve, reject) => {
      this.request(this.isAuth).post(this.entity, this.data)
          .then(response => {
            return resolve(response.data)
          })
          .catch(error => {
            return reject(error)
          })
    })
  }

  get(data) {
    return new Promise((resolve, reject) => {
      this.request(this.isAuth).get(this.entity, data)
          .then(response => {
            return resolve(response.data)
          })
          .catch(error => {
            return reject(error)
          })
    })
  }

  request(isAuth) {
    return new Http(isAuth)
  }
}

export function register(data) {
  return new AuthService('register', data).post()
}

export function confirmEmailRegister(data) {
  return new AuthService(`/register/confirm/${data}`).post()
}

export function confirmPhone(data) {
  return new AuthService('confirm-phone', data).post()
}

export function confirmCode(data) {
  return new AuthService(`confirm-code/${data.code}`, data).post()
}

export function continuePhoneReg(data) {
  return new AuthService('register-phone', data).post()
}

export function logout() {
  return new AuthService('logout', {}, true).get()
}

export function login(data) {
  return new AuthService('login',
      {
        ...data,
        client_id: process.env.client_id,
        client_secret: process.env.client_secret,
        grant_type: 'password'
      }).post()
}

export function setToken(data) {
  document.cookie = `access_token=${data.access_token || ''}`
  document.cookie = `refresh_token=${data.refresh_token || ''}`
  document.cookie = `token_type=${data.token_type || 'Bearer'}`
  document.cookie = `expires_in=${data.expires_in || 0}`
}

export function unsetToken() {
  document.cookie = `access_token=`
  document.cookie = `refresh_token=`
  document.cookie = `token_type=`
  document.cookie = `expires_in=`
}

export function parseToken(data) {
  return {
    ...data,
    expires_in: Math.round((Date.now() + data.expires_in) / 1000)
  }
}

export function getTokenFields() {
  const data = document.cookie.replaceAll(' ', '').split(';');
  let mass = {};
  data.map(value => {
    const key = value.split('=')
    mass[key[0]] = key[1];
  })

  return {
    access_token: mass.access_token || '',
    refresh_token: mass.refresh_token || '',
    token_type: mass.token_type || 'Bearer',
    expires_in: parseInt(mass.expires_in) || 0,
  }
}

export function isExpired(expires_in) {
  if(!expires_in) return false;
  return (Date.now() / 1000) <= expires_in;
}

export function getAccessToken() {
  return Cookies.get('access_token') || ''
}

