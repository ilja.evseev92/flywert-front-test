import axios from 'axios'
import { getLocale } from '../utils'

export default function Http(header) {
  return axios.create({
    baseURL: process.env.base_url,
    headers: {
      'X-Localization': getLocale(),
      ...header
    }
  })
}
