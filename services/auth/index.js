import Cookies from 'js-cookie';
import Http from './http.init';
import jwt from 'jsonwebtoken';
import {getCookieFromReq} from "@helpers/utils";
import {phoneIsValid} from "@services/utils";
import {token} from "@services/token";

class Auth {
  constructor() {
    this.base_url = process.env.base_url;
    this.client_id = process.env.client_id;
    this.client_secret = process.env.client_secret;

    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);
    this.clientAuth = this.clientAuth.bind(this);
    this.serverAuth = this.serverAuth.bind(this);
  }

  request(header) {
    return new Http(header)
  }

  setSession(authResult) {
    const expiresAt = JSON.stringify((authResult.expires_in * 1000) + new Date().getTime())

    Cookies.set('access_token', authResult.access_token);
    Cookies.set('refresh_token', authResult.refresh_token)
    Cookies.set('expiresAt', expiresAt)
  }

  logout() {
    const BearerToken = 'Bearer ' + Cookies.get('access_token');
    //const BearerToken = 'Bearer ' + token;
    return new Promise((resolve, reject) => {
      this.request({Authorization : BearerToken}).get('logout')
          .then(res => {
            Cookies.remove('access_token');
            Cookies.remove('refresh_token');
            Cookies.remove('expiresAt');
            return resolve(res.data)
          })
          .catch(err => {
            return reject(err)
          })
    })
  }

  login(data) {
    return new Promise((resolve, reject) => {
      this.request().post(!!data.phone ? 'login-phone' : 'login-email', {
        ...data,
        client_id: this.client_id,
        client_secret: this.client_secret,
        grant_type: !!data.phone ? 'phone' : 'password'
      })
          .then(res => {
            if(res.data) this.setSession(res.data)
            return resolve(res.data)
          })
          .catch(err => {
            return reject(err)
          })
    })
  }

  loginSocial(data) {
    return new Promise((resolve, reject) => {
      this.request().post('/oauth/token', {
        ...data,
        client_id: this.client_id,
        client_secret: this.client_secret,
        grant_type: "social"
      })
          .then(res => {
            if(res.data) this.setSession(res.data)
            return resolve(res.data)
          })
          .catch(err => {
            return reject(err)
          })
    })
  }

  verifyToken(token) {
    if(token) {
      const decodedToken = jwt.decode(token);
      if(!decodedToken) {return undefined}
      const expiresAt = decodedToken.exp * 1000;

      return (decodedToken && new Date().getTime() < expiresAt) ? decodedToken : undefined
    }
    return undefined;
  }

  clientAuth() {
    const token = Cookies.getJSON('access_token');
    const verifiedToken = this.verifyToken(token);

    if(verifiedToken) {
      return token
    } else {
      return undefined
    }
  }

  serverAuth(req) {
    if(req.headers.cookie) {

      const token = getCookieFromReq(req, 'access_token')
      const verifiedToken = this.verifyToken(token);

      if(verifiedToken) {
        return token
      } else {
        return undefined
      }
    }

    return undefined;
  }
}

const authClient = new Auth();

export default authClient;
