import Http from './http.init'

export class BaseService {

  static get entity() {
    throw new Error('"entity" getter not defined')
  }

  static request(isAuth) {
    return new Http(isAuth)
  }

  static get(params = {}, isAuth) {
    return new Promise((resolve, reject) => {
      return this.request(isAuth).get(`${this.entity}`, params).then(response => {
        return resolve(response.data)
      }).catch(error => {
        return reject(error)
      })
    })
  }

  static post(data, isAuth) {
    return new Promise((resolve, reject) => {
      return this.request(isAuth).post(`${this.entity}`,  data).then(response => {
        return resolve(response.data)
      }).catch(error => {
        return reject(error)
      })
    })
  }
}
