import axios from 'axios'
import {getLocale} from './utils'
import Cookies from 'js-cookie'

export default class Http {
  constructor(isAuth) {
    this.isAuth = !!isAuth
    this.instance = axios.create({
      baseURL: process.env.base_url,
      headers: {
        'X-Localization': getLocale(),
      }
    })

    return this.init()
  }

  init() {
    if (this.isAuth) {
      this.instance.interceptors.request.use(request => {
            request.headers['Authorization'] = 'Bearer ' + Cookies.getJSON('access_token');
          },
          error => {
            return Promise.reject(error)
          }
      )

      return this.instance
    }
    return this.instance
  }
}
