import * as allConst from '../../constants/helpers'

export const helpersSetOneField = (key, payload) => {
  return dispatch => {
    dispatch({
      type: allConst.HELPERS_SET_ONE_FIELD,
      key,
      payload
    })
  }
}

export const clearErrorMessage = () => {
  return dispatch => {
    dispatch({
      type: allConst.CLEAR_ERROR_MESSAGE
    })
  }
}