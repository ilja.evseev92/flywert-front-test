import * as allConst from '../../constants/modal'

export const showModal = (modalType) => {
  return dispatch => {
      dispatch({
        type: allConst.SHOW_MODAL,
        payload: modalType
      })
  }
}

export const hideModal = () => {
  return dispatch => {
      dispatch({
        type: allConst.HIDE_MODAL,
      })
  }
}