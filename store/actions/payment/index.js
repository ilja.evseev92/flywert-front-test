import Cookies from "js-cookie";
import axios from "axios";
import {getLocale} from '@services/utils'
import {getUserData} from '@store/actions/user'
import {SET_PAYMENT_TYPES, SET_PAYMENT_STATUS, SET_PAYMENT_PROCESS_MESSAGE, SET_PAYMENT_LOADING_STATUS} from '@store/constants/payment'
import {SHOW_MODAL, HIDE_MODAL} from '@store/constants/modal'
import {token, tokenLocal} from "@services/token";



const instance = () => {
  return axios.create({
    baseURL: process.env.base_url,
    headers: {
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`,
      //'authorization': 'Bearer ' + token,
      'X-Localization': getLocale(),
    }
  })
}


export const getPaymentType = (id) => async dispatch => {
  if (id !== undefined) {
    return await instance().get(`payments/types/purchase?product=${id}`, {
    }).then(response => {
      if(response.data.success) {
        dispatch({
          type: SET_PAYMENT_TYPES,
          payload: response.data.data,
        })
      }
    }).catch(err => {
      console.log("err", err)
    });
  }
}

export const orderProcess = (data) => async dispatch => {
  dispatch({type: SET_PAYMENT_LOADING_STATUS})
  return await instance().post('types/purchase/orders/process', data).then(response => {
    const resData = response?.data;
    if(resData && resData.success) {
      dispatch({
        type: SET_PAYMENT_STATUS,
        payload: {
          order_item_id: resData.data.id,
          payment_status: resData.data.status
        }
      })
      dispatch({
        type: SET_PAYMENT_PROCESS_MESSAGE,
        payload: {
          status: resData.success,
          message: resData.message
        }
      })
      dispatch({type: HIDE_MODAL})
      dispatch({
        type: SHOW_MODAL,
        payload: 'TICKETS_MODAL'
      })
      getUserData()(dispatch)
    }
  }).catch(error => {
    if(error?.response?.data?.message) {
      setPaymentRequestMessage(false, error.response.data.message)(dispatch)
    }
    console.error("error", error)

  }).finally(() => {
    dispatch({type: SET_PAYMENT_LOADING_STATUS})
  })
}

export const setPaymentRequestMessage = (status = false, message = '') => {
  return dispatch => {
    dispatch({
      type: SET_PAYMENT_PROCESS_MESSAGE,
      payload: {
        status: status,
        message: message
      }
    })
  }
}
