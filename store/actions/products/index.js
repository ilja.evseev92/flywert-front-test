import * as allConst from "../../constants/products";

export const setSelectedProduct = (id) => {
  return dispatch => {
    dispatch({
      type: allConst.SET_SELECTED_PRODUCT,
      payload: id || ''
    })
  }
}