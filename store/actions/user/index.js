import axios from 'axios';
import Cookies from 'js-cookie'
import {getCookieFromReq} from '@helpers/utils'
import {SET_USER_DATA} from '../../constants/user'
import {showModal} from '../modal'
import {helpersSetOneField} from '../helpers'

const setAuthHeader = (req) => {
  const token = req ? getCookieFromReq(req, 'access_token') : Cookies.getJSON('access_token');

  if (token) {
    return {
      headers: {
        'authorization': `Bearer ${token}`,
        'X-Localization': 'ru',
      }
    }
  }

  return undefined;
}

export const getSecretData = async (req) => {
  return await axios.get(`https://flywert-dev.ru/api/v1/user`, setAuthHeader(req)).then(response => response.data);
}

const instance = axios.create({
  baseURL: process.env.base_url,
  headers: {
    'authorization': `Bearer ${Cookies.getJSON('access_token')}`,
    'X-Localization': 'ru',
  }
})

export const getUserData = () => async dispatch => {
  return await instance.get('user', {
    headers: {
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`
    }
  }).then(response => {
    dispatch({
      type: SET_USER_DATA,
      payload: response.data.data
    })
  }).catch(err => {
    console.log("err", err)
  });
}

export const uploadUserAvatar = (data) => async dispatch => {
  const dataForm = new FormData();
  dataForm.append('avatar', data);

  return await instance.post(`user/avatar`, dataForm, {
    headers: {
      'Content-Type': `multipart/form-data; boundary=${dataForm._boundary}`,
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`
    }
  }).then(response => {
    if (response?.data?.success) {
      getUserData()(dispatch)
    }
  }).catch(err => {
    const errorData = err?.response?.data?.errors?.avatar;
    if (errorData) {
      helpersSetOneField('errorMessage', errorData[0] || '')(dispatch)
      showModal('ERROR_MODAL')(dispatch)
    }
  });
}

export const removeUserAvatar = () => async dispatch => {
  return await instance.post(`user/avatar`, {}, {
    headers: {
      'Content-Type': 'multipart/form-data',
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`
    }
  }).then(response => {
    if (response?.data?.success) {
      getUserData()(dispatch)
    }
  }).catch(err => {
    console.log("err", err)
  });
}

export const updateUserInfo = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
  const userInfo = {...data.values}
  delete userInfo.email

  return await instance.patch('user', userInfo, {
    headers: {
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`
    }
  }).then(response => {
    if(response.data.success) {
      getUserData()(dispatch)
      helpersSetOneField('success', 'profileInfo')(dispatch)
      setTimeout(() => {
        helpersSetOneField('success', '')(dispatch)
      }, 2000);
    }
    if (data.values.email !== data.email) {
      updateUserEmail({email: data.values.email})(dispatch)
    }
  }).catch(err => {
    helpersSetOneField('success', '')(dispatch)
    console.log("err", err)
  }).finally(() => {
    helpersSetOneField('loading', false)(dispatch)
  });
}


export const updateUserEmail = (data) => async dispatch => {
  const reqData = new URLSearchParams(Object.keys(data).map(key => [key, data[key]]))
  return await instance.patch('user/email', reqData, {
    headers: {
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  }).then(response => {
    console.log(response)
  }).catch(err => {
    const errorData = err?.response?.data?.message;
    if (errorData) {
      helpersSetOneField('errorMessage', errorData || '')(dispatch)
      showModal('ERROR_MODAL')(dispatch)
    }
  })
}

export const updatePassword = (data) => async dispatch => {
  helpersSetOneField('loading', true)(dispatch)
  return await instance.patch('user/password', data, {
    headers: {
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`
    }
  }).then(response => {
    if(response.data.success) {
      helpersSetOneField('success', 'updatePassword')(dispatch)
      setTimeout(() => {
        helpersSetOneField('success', '')(dispatch)
      }, 2000);
    }
  }).catch(err => {
    const errorData = err?.response?.data?.message;
    helpersSetOneField('success', '')(dispatch)
    if (errorData) {
      helpersSetOneField('errorMessage', errorData || '')(dispatch)
      showModal('ERROR_MODAL')(dispatch)
    }
  }).finally(() => {
    helpersSetOneField('loading', false)(dispatch)
  });
}


export const getUserOrders = () => async dispatch => {
  return await instance.get('get-order-items', {
    headers: {
      'authorization': `Bearer ${Cookies.getJSON('access_token')}`
    }
  }).then(response => response.data).catch(err => {
    console.log("err", err)
  });
}



