import * as allConst from '../../constants/auth'
import {HIDE_MODAL} from '../../constants/modal'

const initialState = {
  isAuth: false,
  loading: false,
  message: '',
  requestStatus: false,
  registerActiveForm: 'sms',
  confirmPhoneStatus: true,
  confirmCodeStatus: false,
  confirmEmailStatus: false,
  confirmPhone: '',
  confirmCode: '',
  phoneRegActiveForm: 'phone',
  emailRegStatus: false
}

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case allConst.SET_TOKEN: {
      return {
        ...state,
        ...action.payload,
        isAuth: true
      }
    }
    case allConst.UNSET_TOKEN: {
      return {
        ...state,
        access_token: '',
        refresh_token: '',
        expires_in: 0,
        isAuth: false
      }
    }
    case allConst.AUTH_SET_ONE_FIELD: {
      return {
        ...state,
        [action.key]: action.payload
      }
    }
    case HIDE_MODAL: {
      return {
        ...state,
        message: '',
        requestStatus: false,
      }
    }
    case allConst.CLEAR_MESSAGE: {
      return {
        ...state,
        message: '',
        requestStatus: false,
      }
    }
    case allConst.CLEAR_REGISTER_DATA: {
      return {
        ...state,
        confirmPhone: '',
        confirmCode: '',
        phoneRegActiveForm: 'phone',
        emailRegStatus: false,
        message: '',
        requestStatus: false,
        registerActiveForm: 'sms',
      }
    }
    default:
      return state;
  }
}

export default authReducer;