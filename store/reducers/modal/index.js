import * as allConst from '../../constants/modal'

const initialState = {
  modalType: null,
}

export default function Modal(state = initialState, action) {
  switch (action.type) {
    case allConst.SHOW_MODAL:
      return {
        ...state,
        modalType: action.payload,
      }
    case allConst.HIDE_MODAL:
      return initialState
    default:
      return state
  }
}