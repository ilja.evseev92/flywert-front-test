import { combineReducers } from "redux";
import auth from "./auth";
import modal from "./modal";
import products from  "./products"
import helpers from "./helpers"
import user from "./user"
import chat from './chat'
import tickets from './tickets'
import payment from './payment'
import product from './products'

export default combineReducers({
  auth,
  modal,
  products,
  helpers,
  user,
  chat,
  tickets,
  payment,
  product
});