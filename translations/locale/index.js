import ru from './ru'
import de from './de'
import en from './en'

export default {
  ru,
  de,
  en
}